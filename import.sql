CREATE TABLE alertconfig (
    alert_id integer NOT NULL,
    deviceid character varying(255) NOT NULL,
    freefld1 character varying(255),
    freefld2 character varying(255),
    freefld3 character varying(255),
    max_val character varying(255),
    min_val character varying(255),
    r_mod_time character varying(255),
    deleted boolean NOT NULL,
    exact_val character varying(255),
    property_id character varying(255) NOT NULL,
    r_cre_time character varying(255) NOT NULL,
    target_url character varying(255)
);

CREATE TABLE deviceconfig (
    deviceid character varying(255) NOT NULL,
    data character varying(255),
    freefld1 character varying(255),
    freefld2 character varying(255),
    freefld3 character varying(255),
    id integer NOT NULL,
    loginid character varying(255),
    r_mod_time character varying(255),
    deleted boolean NOT NULL,
    r_cre_time character varying(255) NOT NULL
);

CREATE TABLE devicetabconfig (
    id integer NOT NULL,
    deviceid character varying(255) NOT NULL,
    freefld1 character varying(255),
    freefld2 character varying(255),
    freefld3 character varying(255),
    r_mod_time character varying(255),
    tabname character varying(255) NOT NULL,
    deleted boolean DEFAULT 0 NOT NULL,
    disabled boolean DEFAULT 0 NOT NULL,
    graph_one character varying(255),
    graph_one_alert boolean DEFAULT 0 NOT NULL,
    graph_one_color character varying(255),
    graph_three character varying(255),
    graph_three_alert boolean DEFAULT 0 NOT NULL,
    graph_three_color character varying(255),
    graph_two character varying(255),
    graph_two_alert boolean DEFAULT 0 NOT NULL,
    graph_two_color character varying(255),
    no_of_chart integer NOT NULL,
    r_cre_time character varying(255) NOT NULL
);

insert into deviceconfig (deviceid, r_cre_time,id,deleted) values ('IoT-01', 'Fri May 5 2017 12:18:44',0,'0');
insert into devicetabconfig (deviceid, r_cre_time,id,deleted,tabname,disabled,no_of_chart,graph_one,graph_two,graph_three,graph_one_color,graph_two_color,graph_three_color,graph_one_alert,graph_two_alert,graph_three_alert) values ('IoT-01', 'Fri May 5 2017 12:18:44',0,'0','Oil','0',3,'oil_temp','oil_pressure', 'fuel_flow', '#FF0000', '#E9967A', '#483D8B','0','0','0');
insert into devicetabconfig (deviceid, r_cre_time,id,deleted,tabname,disabled,no_of_chart,graph_one,graph_two,graph_three,graph_one_color,graph_two_color,graph_three_color,graph_one_alert,graph_two_alert,graph_three_alert) values ('IoT-01', 'Fri May 5 2017 12:18:44',1,'0','Engine','0',3,'eng_temp','eng_vib', 'eng_noise', '#FFFF00', '#9400D3', '#1E90FF','0','0','0');
insert into devicetabconfig (deviceid, r_cre_time,id,deleted,tabname,disabled,no_of_chart,graph_one,graph_two,graph_one_color,graph_two_color,graph_one_alert,graph_two_alert) values ('IoT-01', 'Fri May 5 2017 12:18:44',2,'0','acceleration data','0',2,'rpm','acceleration', '#00FF00', '#f47f0e','0','0');
insert into devicetabconfig (deviceid, r_cre_time,id,deleted,tabname,disabled,no_of_chart,graph_one,graph_two,graph_three,graph_one_color,graph_two_color,graph_three_color,graph_one_alert,graph_two_alert,graph_three_alert) values ('IoT-01', 'Fri May 5 2017 12:18:44',3,'0','Energy','0',3,'kers_battery','generator', 'fuel_consumption', '#00FFFF', '#008000', '#CD5C5C','0','0','0');
insert into devicetabconfig (deviceid, r_cre_time,id,deleted,tabname,disabled,no_of_chart,graph_one,graph_two,graph_three,graph_one_color,graph_two_color,graph_three_color,graph_one_alert,graph_two_alert,graph_three_alert) values ('IoT-01', 'Fri May 5 2017 12:18:44',4,'0','Miscellaneous','0',3,'exh_temp','rdt', 'woc', '#0000FF', '#F08080', '#9370DB','0','0','0');
