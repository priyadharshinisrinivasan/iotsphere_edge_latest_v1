function NewGateCtrl(scope, reg, http) {
	scope.showmsg = false;
	scope.message = "";
	scope.msg_icon = "";
	scope.newgateway = {};
	scope.devreg = function(e) {
		scope.message = JSON.stringify(e.data);
		message = (e.data.Status).replace(/\\"/g, '"');
		message = JSON.parse(message);
		if(message.registered == true){
			scope.msg_icon = "greentick.png";
			scope.message = "Gateway registered successfully";
		}
		else{
			scope.msg_icon = "failed.png";
			scope.message = "Error while registering gateway";
		}
		scope.showmsg = true;
		scope.newgateway = {};
		document.getElementById("gatewayRegLoading").style.display = "none";
		document.getElementById("gatewayRegistrationContent").style.display = "block";
	}
	scope.submit = function() {
		document.getElementById("gatewayRegLoading").style.display = "block";
		document.getElementById("gatewayRegistrationContent").style.display = "none";
		scope.submitted = true;
		var fd = new FormData();
		fd.append('gateway', JSON.stringify(scope.newgateway));
		reg.register(fd, "registergateway", scope.devreg)

	};
}

angular.module("IoTSphereEG").controller("NewGateCtrl",
		[ "$scope", "registration", "$http", NewGateCtrl ]);