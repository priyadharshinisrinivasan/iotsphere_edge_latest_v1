angular.module("IoTSphereEG").config(
		[ "$stateProvider", "$urlRouterProvider", function(t, e) {
			e.otherwise("/"), t.state("index", {
				url : "/",
				templateUrl : "templates/index",
				controller: "homeCtrl",
			}).state("dashboard", {
				url : "/dashboard",
				templateUrl : "templates/dashboard",
			}).state("about", {
				url : "/about",
				templateUrl : "templates/about"
			}).state("support", {
				url : "/support",
				templateUrl : "templates/support"
			}).state("search", {
				url : "/search",
				templateUrl : "templates/search"
			}).state("gateway", {
				url : "/gateway/{id}",
				templateUrl : function($stateParams) {
					return "templates/gateway/" + $stateParams.id
				},
				controller: "gatewayCtrl",
				resolve : {
					param : function($stateParams) {
						return $stateParams.id;
					}
				}
			}).state("livedata", {
				url : "/livedata",
				templateUrl : "templates/livedata"
			}).state("newdevice", {
				url : "/newdevice",
				templateUrl : "templates/newdevice"
			}).state("newgateway", {
				url : "/newgateway",
				templateUrl : "templates/newgateway"
			})
		} ]);