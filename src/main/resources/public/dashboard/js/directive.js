function rdLoading() {
	var d = {
		restrict : "AE",
		template : '<div class="loading"><div class="double-bounce1"></div><div class="double-bounce2"></div></div>'
	};
	return d
}
angular.module("IoTSphereEG").directive("rdLoading", rdLoading);

function rdWidgetBody() {
	var d = {
		requires : "^rdWidget",
		scope : {
			loading : "=?",
			classes : "@?"
		},
		transclude : !0,
		template : '<div class="widget-body" ng-class="classes"><rd-loading ng-show="loading"></rd-loading><div ng-hide="loading" class="widget-content" ng-transclude></div></div>',
		restrict : "E"
	};
	return d
}
angular.module("IoTSphereEG").directive("rdWidgetBody", rdWidgetBody);

function rdWidgetFooter() {
	var e = {
		requires : "^rdWidget",
		transclude : !0,
		template : '<div class="widget-footer" ng-transclude></div>',
		restrict : "E"
	};
	return e
}
angular.module("IoTSphereEG").directive("rdWidgetFooter", rdWidgetFooter);

function rdWidgetTitle() {
	var i = {
		requires : "^rdWidget",
		scope : {
			title : "@",
			icon : "@"
		},
		transclude : !0,
		template : '<div class="widget-header"><div class="row"><div class="pull-left"><i class="fa" ng-class="icon"></i> {{title}} </div><div class="pull-right col-xs-6 col-sm-4" ng-transclude></div></div></div>',
		restrict : "E"
	};
	return i
}
angular.module("IoTSphereEG").directive("rdWidgetHeader", rdWidgetTitle);

function rdWidget() {
	var d = {
		transclude : !0,
		template : '<div class="widget" ng-transclude></div>',
		restrict : "EA"
	};
	return d
}
angular.module("IoTSphereEG").directive("rdWidget", rdWidget);