function DeviceCtrl(scope, http) {
	
	scope.getPort = function() {
		var http = new XMLHttpRequest();
		var url = "port";
		http.open("POST", url, true);
		// Send the proper header information along with the request
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");

		http.onreadystatechange = function() {// Call a function when the
			// state changes.
			if (http.readyState == 4 && http.status == 200) {
				scope.port = http.responseText;
				scope.port = scope.porscope.replace(/\\"/g, '"');
				scope.port = JSON.parse(scope.port);
				console.log("getPort " + scope.port);
			}
		}

		http.send("dummy=dummy");
	}
	scope.connect = "Connect";
	scope.connect_button = function() {

		if (scope.connect == "Connect") {
			if (websockescope.readyState == websockescope.CLOSED)
				websocket = new WebSocket(wsUri);
			scope.connect = "Connecting..";
			websockescope.send(JSON.stringify({
				'action' : {
					'type' : 'connect',
					'comport' : scope.comp.port,
				}
			}));
			
			scope.connect = "Disconnect";
		} else if (scope.connect == "Disconnect") {
			websockescope.send(JSON.stringify({
				'action' : {
					'type' : 'disconnect',
					'comport' : scope.comp.port,
				}
			}))
			scope.connect = "Connect";
			scope.uri_text = '';
			scope.devices[0].device_table_data["devstatus"] = "Offline";
		}
	};
	scope.d_settings = function() {
		scope.getPort();
		scope.calibration.Device_Id = scope.devices[scope.currentDeviceIndex].name;
		nd.openConfirm({
			template : 'd_settings',
			className : 'ngdialog-theme-default',
			height : 'auto',
			overflow : 'auto',
			width : '40%',
			scope : t
		}).then(function(value) {
			var fd = new FormData();
		       fd.append('Device_Id',scope.calibration.Device_Id);
		       fd.append('property',scope.calibration.property);
		       fd.append('value',scope.calibration.value);
		       h.post("save_calibration", fd, {
		          transformRequest: angular.identity,
		          headers: {'Content-Type': undefined}
		       })
		       .success(function(e){
		    	   console.log("here");
		    	   console.log(e);
		       })
		       .error(function(e){
		       });
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {
			console.log('rejected:' + value);

		});
	};

}
angular.module("IoTSphereEG").controller("DeviceCtrl",
		[ "$scope", "$http", DeviceCtrl ]);