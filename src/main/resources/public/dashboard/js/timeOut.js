// Set timeout variables.
var timoutWarning = 0; // Display warning in 14 Mins.840000
var timoutNow = 0; // Timeout in 15 mins.900000
var logoutUrl = '/logout'; // URL to logout page.

var warningTimer;
var timeoutTimer;

// Start timers.
function StartTimers(appMaxIdleTime) {

	//console.log("appMaxIdleTime" + appMaxIdleTime);

	if (appMaxIdleTime > 60000) {
		
		timoutNow = appMaxIdleTime;
		timoutWarning = timoutNow - 60000;

		warningTimer = setTimeout("IdleWarning()", timoutWarning);
		timeoutTimer = setTimeout("IdleTimeout()", timoutNow);

	}
	
	
}

function showAndHideSessionPopUp() {

	refreshApplication();
	
	// Get the modal
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	//var btn = document.getElementById("myBtn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal
	//btn.onclick = function() {
		modal.style.display = "block";

		var sec = 59;
		var timer = setInterval(function() {
			document.getElementById("seconds").innerHTML = pad(sec--);
		}, 1000);

		setTimeout(function() {
			clearInterval(timer);
		}, 60000);
	//}

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
		refreshApplication();
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
			refreshApplication();
		}
	}
}
function pad(val) {
    return val > 9 ? val : "0" + val;
}

// Reset timers.
function ResetTimers(appMaxIdleTime) {
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    StartTimers(appMaxIdleTime);
}

// Show idle timeout warning dialog.
function IdleWarning() {
	//alert("Your session is about to expire. Please click Ok to continue session.");
	var myWindow = window.open();
    myWindow.close();
	showAndHideSessionPopUp();
	
}

function refreshApplication()
{
	var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	     
	    	//var respValue = xhttp.responseText;
			//console.log(respValue);
	    }
	  };
	  
	  var url = "edge/refreshServer";
		//console.log(url);
		
	  xhttp.open("POST", url, true);
	  xhttp.send();
}

// Logout the user.
function IdleTimeout() {
	window.location.replace(logoutUrl);
	window.location.href = logoutUrl;
    window.location = logoutUrl;
}
