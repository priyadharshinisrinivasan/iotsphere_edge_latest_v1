angular.module("IoTSphereEG").service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl,callback){
       var fd = new FormData();
       fd.append('file', file);
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       }).then(function(e){
    	   callback(e);
       }, function(e){
    	   callback(e)
       });
    }
    this.fetch_dev_data = function(uploadUrl,getdevId,callbackresult){
    	var devId = getdevId();
    	if(devId != null){
	       $http.get(uploadUrl+devId).then(function(e){
	    	   // console.log(JSON.stringify(e.data));
	    	   callbackresult(e.data);
	       }, function(e){
	    	   console.log("Error"+e)
	       });
	    }
    }
    
 }]);
angular.module("IoTSphereEG").factory('websock', [function ($http) {
return(
	    connectws = function() {
			var socket = new SockJS('/ws');
			stompClient = Stomp.over(socket);
			stompClient.debug = () => {};
			return stompClient;
		},
		getproperty = function(dev, callback){
			var http = new XMLHttpRequest();
			var url = "get_calibration";
			http.open("POST", url, true);
			http.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");

			http.onreadystatechange = function() {// Call a function when the
				if (http.readyState == 4 && http.status == 200) {
					var response = http.responseText;
					callback(response);
				}
			}
			http.send("Device_Id="+dev);
		},
		getpropertyvalue = function(dev, property,callback){
			var http = new XMLHttpRequest();
			var url = "get_calibrationvalue";
			http.open("POST", url, true);
			http.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");

			http.onreadystatechange = function() {// Call a function when the
				if (http.readyState == 4 && http.status == 200) {
					var response = http.responseText;
					callback(response);
				}
			}
			http.send("Device_Id="+dev+"&property="+property);
		}
		);
}]);
angular.module("IoTSphereEG").service('getgateway', [function ($http) {
this.getgateway = function(callback) {
	var http = new XMLHttpRequest();
	var url = "getgateways";
	http.open("POST", url, true);
	http.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	http.onreadystatechange = function() {// Call a function when the
		if (http.readyState == 4 && http.status == 200) {
			gateway = http.responseText;
			gateway = gateway.replace(/\\"/g, '"');
			gateway = JSON.parse(gateway);
			callback(gateway);
		}
	}
	http.send("dummy=dummy");
}
}]);
angular.module("IoTSphereEG").service('registration', ['$http', function ($http) {
    this.register = function(data, uploadUrl,callback){
       $http.post(uploadUrl, data, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       }).then(function(e){
    	   callback(e);
       }, function(e){
    	   callback(e)
       });
    }
}]);