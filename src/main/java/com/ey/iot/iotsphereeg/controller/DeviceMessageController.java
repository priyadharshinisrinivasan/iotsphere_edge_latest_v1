package com.ey.iot.iotsphereeg.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import com.ey.iot.iotsphereeg.model.DeviceMessage;

@Controller
public class DeviceMessageController {

    @MessageMapping("/chat.sendMessage")
    @SendTo("/channel/public")
    public DeviceMessage sendMessage(@Payload DeviceMessage deviceMessage) {
        return deviceMessage;
    }

    @MessageMapping("/chat.addDevice")
    @SendTo("/channel/public")
    public DeviceMessage addUser(@Payload DeviceMessage deviceMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("deviceId", deviceMessage.getSender());
        return deviceMessage;
    }

}
