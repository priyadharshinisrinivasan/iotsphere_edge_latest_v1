package com.ey.iot.iotsphereeg.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.iot.iotsphereeg.model.PortConnectionDetailsModel;
import com.ey.iot.iotsphereeg.service.PortConnectionDetailsService;
import com.ey.iot.iotsphereeg.utility.Constants;
import com.ey.iot.iotsphereeg.utility.DeviceManagement;
import com.ey.iot.iotsphereeg.utility.Main_Edge;

@RestController
@RequestMapping("/edge")
public class EdgeController {

	@Autowired
	Main_Edge main_Edge;

	@Autowired
	PortConnectionDetailsService portConnectionDetailsService;
	
	@Autowired
	DeviceManagement deviceManagement;
	@Autowired
	@Value("${GWY_ID}")
	private String gatewayid;
	Logger logger = Logger.getLogger(this.getClass());
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/port", method = RequestMethod.POST)
	public @ResponseBody JSONObject port() {
		// main_edgeobj = new Main_Edge();
		String portlist[] = Main_Edge.showport().split(",");
		JSONObject port = new JSONObject();
		port.put("port", portlist);
		return port;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/connect" }, method = { RequestMethod.POST })
	public @ResponseBody String connect(@RequestParam("port") String port,
			@RequestParam("selectedDeviceId") String selectedDeviceId, HttpServletRequest request) throws Exception {
		System.out.println("Connect " + port + " selectedDeviceId " + selectedDeviceId);
		JSONObject status = new JSONObject();

		String loginId = (String) request.getSession().getAttribute("LoggedInUserName");
		String connectionStatus = null;
        boolean deviceregistered=false;
		try {
			if (deviceManagement.isDeviceRegisted(selectedDeviceId)) {
				deviceregistered=true;
			}
			deviceregistered=true;//added for dgs
		}
			catch (Exception e) {
				//e.printStackTrace();
				status.put("connectionError", "Error while connecting to IoTHub/Internet");
				status.put("iothubonlinestatus", "false");
				logger.info("Error while connecting to IoTHub/Internet");
			}
		
		try
		{
		if(deviceregistered)
		{
				if (!main_Edge.stopThread) {

					main_Edge.stopThread = false;
					main_Edge.thread = new Thread(main_Edge);
					main_Edge.port = port;
					main_Edge.selectedDeviceId = selectedDeviceId;
					main_Edge.loginId = loginId;
					main_Edge.thread.start();

				} else {
					main_Edge.stopThread = false;
					// status.put("Status", "Already Running");
					main_Edge.thread = new Thread(main_Edge);
					main_Edge.port = port;
					main_Edge.selectedDeviceId = selectedDeviceId;
					main_Edge.loginId = loginId;
					main_Edge.thread.start();

				}
				connectionStatus = Constants.CONNECTED_STATUS;
				updateConnectionStatus(loginId, port, connectionStatus);
				status = getConnectionStatus(status, loginId, port);	
			}
		} catch (Exception e) {
			e.printStackTrace();
			status.put("connectionError", "Error while connecting to COM Port");
			logger.info("Error while connecting to COM Port");
			connectionStatus = Constants.DISCONNECTED_STATUS;
			updateConnectionStatus(loginId, port, connectionStatus);
			status = getConnectionStatus(status, loginId, port);	
		}
		return status.toJSONString();
	}

	private JSONObject getConnectionStatus(JSONObject status, String loginId, String port) {

		PortConnectionDetailsModel portConnectionDetails = getPortConnectionDetails(loginId, port);

		String portStatus = null;
		String connectionStartTime = null;
		String connectionEndTime = null;
		if (null != portConnectionDetails) {

			portStatus = portConnectionDetails.getConnectionStatus();
			connectionStartTime = portConnectionDetails.getConnectionStartTime();
			connectionEndTime = portConnectionDetails.getConnectionEndTime();
		}

		status.put("portStatus", portStatus);
		status.put("connectionStartTime", connectionStartTime);
		status.put("connectionEndTime", connectionEndTime);
		status.put("iothubonlinestatus", "true");
		return status;
	}

	private void updateConnectionStatus(String loginId, String port, String connectionStatus) {
        
		PortConnectionDetailsModel portConnectionModel = new PortConnectionDetailsModel();
		portConnectionModel.setLoginId(loginId);
		portConnectionModel.setPort(port);//unique
		portConnectionModel.setGatewayId(gatewayid);//non unique
		portConnectionModel.setConnectionStatus(connectionStatus);
		if (Constants.CONNECTED_STATUS.equals(connectionStatus)) {
			portConnectionModel.setConnectionStartTime(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
			portConnectionModel.setConnectionEndTime(null);
		} else {
			portConnectionModel.setConnectionEndTime(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
		}
		portConnectionModel.setrModTime(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
		portConnectionDetailsService.saveOrUpdate(portConnectionModel);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/disconnect" }, method = { RequestMethod.POST })
	public @ResponseBody String disconnect(@RequestParam("port") String port,
			@RequestParam("selectedDeviceId") String selectedDeviceId, HttpServletRequest request) {
		
		String loginId="DummyLogin";
		if(request!=null)
		{
		 loginId = (String) request.getSession().getAttribute("LoggedInUserName");
		}
		String connectionStatus = null;
		
		JSONObject status = new JSONObject();
		
		try {
				main_Edge.port = port;
				main_Edge.selectedDeviceId = selectedDeviceId;

				if (main_Edge.stopThread) {
					// status.put("Status", Constants.NOT_RUNNING);
					
					status = getConnectionStatus(status, loginId, port);

				} else {
					main_Edge.stopThread = true;				
					try {
						main_Edge.disconnect();
						main_Edge.thread.join();
					} catch (InterruptedException e) {
						// e.printStackTrace();
						System.out.println(e);
					}

					connectionStatus = Constants.DISCONNECTED_STATUS;

					updateConnectionStatus(loginId, port, connectionStatus);

					status = getConnectionStatus(status, loginId, port);
				}
		
		} catch (Exception e) {
			e.printStackTrace();
			status.put("connectionError", "Error while Disconnecting COM port");
			logger.info("Error while Disconnecting COM port");
		}
		return status.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/portStatus" }, method = { RequestMethod.POST })
	public @ResponseBody String getPortStatus(@RequestParam("port") String port, HttpServletRequest request) {
		JSONObject status = new JSONObject();

		String loginId = (String) request.getSession().getAttribute("LoggedInUserName");

		PortConnectionDetailsModel portConnectionDetails = getPortConnectionDetails(loginId, port);
		String portStatus = null;
		String connectionStartTime = null;
		String connectionEndTime = null;
		if (null != portConnectionDetails) {

			portStatus = portConnectionDetails.getConnectionStatus();
			connectionStartTime = portConnectionDetails.getConnectionStartTime();
			connectionEndTime = portConnectionDetails.getConnectionEndTime();
		}

		status.put("portStatus", portStatus);
		status.put("connectionStartTime", connectionStartTime);
		status.put("connectionEndTime", connectionEndTime);

		return status.toJSONString();
	}

	private PortConnectionDetailsModel getPortConnectionDetails(String loginId, String port) {

		List<PortConnectionDetailsModel> portConectionDetails = portConnectionDetailsService
				.getPortConnectionDetails(port);

		if (null != portConectionDetails && portConectionDetails.size() > 0) {
			return portConectionDetails.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/refreshServer" }, method = { RequestMethod.POST })
	public @ResponseBody String refreshServer() {
		JSONObject status = new JSONObject();

		status.put("Status", "Refreshed the server");

		
		return status.toJSONString();
	}
	
	@PreDestroy
	public void onServerShutdown() throws Exception {
		// Update all the port status to disconnected
		portConnectionDetailsService.updateAllPortStatusToDisconnected();
	}
	
	
	@PostConstruct
	public void onServerStart() throws Exception {
		// Update all the port status to disconnected
		portConnectionDetailsService.updateAllPortStatusToDisconnected();
	}
}
