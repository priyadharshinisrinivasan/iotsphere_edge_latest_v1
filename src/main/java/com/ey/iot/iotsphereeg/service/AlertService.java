package com.ey.iot.iotsphereeg.service;

import java.util.List;

import com.ey.iot.iotsphereeg.model.AlertModel;

public abstract interface AlertService
{
  public abstract void savedata(AlertModel paramAlertModel);
  
  public abstract void deletedata(String paramString);
  
  public abstract List<AlertModel> finddata(String paramString);
  
}
