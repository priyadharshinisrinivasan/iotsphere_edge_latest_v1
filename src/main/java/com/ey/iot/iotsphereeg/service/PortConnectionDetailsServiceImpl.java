package com.ey.iot.iotsphereeg.service;

import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotsphereeg.dao.PortConnectionDetailsDao;
import com.ey.iot.iotsphereeg.model.PortConnectionDetailsModel;

@Service("PortConnectionDetailsService")
@DynamicUpdate(true)
@Transactional
public class PortConnectionDetailsServiceImpl implements PortConnectionDetailsService {
	@Autowired
	private PortConnectionDetailsDao dao;

	@Override
	public void saveOrUpdate(PortConnectionDetailsModel portConnectionDetails) {
		this.dao.saveOrUpdate(portConnectionDetails);
	}

	@Override
	public List<PortConnectionDetailsModel> getPortConnectionDetails(String port) {
		return dao.getPortConnectionDetails(port);
	}

	@Override
	public void updateAllPortStatusToDisconnected() {
		dao.updateAllPortStatusToDisconnected();
	}

}
