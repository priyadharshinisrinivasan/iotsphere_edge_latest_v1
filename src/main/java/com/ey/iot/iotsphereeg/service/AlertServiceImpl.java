package com.ey.iot.iotsphereeg.service;


import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotsphereeg.dao.AlertDao;
import com.ey.iot.iotsphereeg.model.AlertModel;

@Service("alertService")
@DynamicUpdate(true)
@Transactional
public class AlertServiceImpl
  implements AlertService
{
  @Autowired
  private AlertDao dao;
  
  public void savedata(AlertModel AlertModel)
  {
    this.dao.saveOrUpdate(AlertModel);
  }
  
  public List<AlertModel> finddata(String input)
  {
    return this.dao.finddata(input);
  }
  
  
  public void deletedata(String ssn)
  {
    this.dao.deletedata(ssn);
  }

 
}
