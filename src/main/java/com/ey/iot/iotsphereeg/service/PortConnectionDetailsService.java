package com.ey.iot.iotsphereeg.service;

import java.util.List;

import com.ey.iot.iotsphereeg.model.PortConnectionDetailsModel;

public abstract interface PortConnectionDetailsService {
	public abstract void saveOrUpdate(PortConnectionDetailsModel deviceDetails);

	public abstract List<PortConnectionDetailsModel> getPortConnectionDetails( String port);
	
	public void updateAllPortStatusToDisconnected();


}
