package com.ey.iot.iotsphereeg.service;

import java.util.List;

import com.ey.iot.iotsphereeg.model.GatewayModel;

public abstract interface GatewayService {
	public abstract List<GatewayModel> getAllGateway();

	public abstract List<GatewayModel> getGatewayById(String GatewayId);

	public abstract boolean addGateway(GatewayModel gatewayModel);

	public abstract void updateGateway(GatewayModel gatewayModel);

	public abstract void deleteGateway(String gatewayID);

	public abstract List<GatewayModel> searchGateway(String keyword);

	public abstract GatewayModel getGateway(String gatewayID);

	public abstract boolean isGatewayExist(String string);

	public abstract void saveOrUpdate(GatewayModel gateway);
}