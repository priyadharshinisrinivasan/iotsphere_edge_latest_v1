package com.ey.iot.iotsphereeg.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;

import com.ey.iot.iotsphereeg.encrypt.BCryptutil;
import com.ey.iot.iotsphereeg.model.LoginModel;

public class UserRegister {

	public static String register(EYService eyservice, final ModelMap model, HttpServletRequest request,
			@RequestParam("email") String emailuser, @RequestParam("password") String password,
			@RequestParam("fullname") String fullname, @RequestParam("city") String city,
			@RequestParam("country") String country, @RequestParam("address") String address) {
		String msg;

		List<LoginModel> LoginModelcheck = eyservice.finddata(emailuser);
		// check is email already exists in db for any record
		if (LoginModelcheck.isEmpty() == false && (LoginModelcheck.get(0).getLoginid() != null)) {
			if (LoginModelcheck.get(0).getLoginid().length() > 0) {
				model.addAttribute("error", "true");
				model.addAttribute("msg3", "User is already Registered!");
				msg = "User is already Registered!";
				return msg;
			}
			return "User is already Registered";
		} else {

			String computedhash = BCryptutil.encrypt(password);
			boolean flg = BCryptutil.verifypwd(password, computedhash);// insert into db
			try {
				if (flg == true) {
					LoginModel LoginModel = new LoginModel();
					LoginModel.setAddress(address);
					LoginModel.setCity(city);
					LoginModel.setCountry("IN");
					LoginModel.setLoginid(emailuser);
					LoginModel.setName(fullname);
					LoginModel.setPwd(computedhash);
					LoginModel.setFreefld1("");
					LoginModel.setFreefld2("");
					LoginModel.setFreefld3("");

					DateTimeZone timeZoneIndia = DateTimeZone.forID("Asia/Calcutta");
					DateTime nowIndia = DateTime.now(timeZoneIndia);
					Date dt = nowIndia.toLocalDateTime().toDate();
					LoginModel.setRModTime(dt);
					LoginModel.setRcretime(dt);
					eyservice.savedata(LoginModel);
					model.addAttribute("error", "true");
					model.addAttribute("msg3", "Registration successfull");
					msg = "Registration Successfull!";
					return msg;
				} else {
					model.addAttribute("error", "true");
					model.addAttribute("msg3", "Registration unsuccessfull");
					msg = "Registration Unsuccessfull!";
					return msg;
				}
			} catch (Exception e) {
				model.addAttribute("error", "true");
				model.addAttribute("msg3", "Error processing Request");
				msg = "Registration Unsuccessfull!";
				return msg;
			}

		}

	}
}
