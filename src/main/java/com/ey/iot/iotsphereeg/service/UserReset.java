package com.ey.iot.iotsphereeg.service;
import java.security.SecureRandom;
import java.util.*;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.ey.iot.iotsphereeg.encrypt.BCryptutil;
import com.ey.iot.iotsphereeg.model.LoginModel;

public class UserReset {

	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();
	
	public static String reset(String emailuser,EYService eyservice)
	{
		   String flg="false";
	     try
	     {
	     List<LoginModel> LoginModel = eyservice.finddata(emailuser);

			if(LoginModel.isEmpty()==false)
			{
				String resetpwd=randomString();
				String computedhash=BCryptutil.encrypt(resetpwd);
				DateTimeZone timeZoneIndia = DateTimeZone.forID( "Asia/Calcutta" );
				DateTime nowIndia = DateTime.now( timeZoneIndia );
				Date dt=nowIndia.toLocalDateTime().toDate();		
					LoginModel.get(0).setRModTime(dt);
				 LoginModel.get(0).setPwd(computedhash);
				 eyservice.savedata(LoginModel.get(0));   
			     System.out.println("saved record");
			}
	     }
	     catch (Exception e)
	     {
	    	 flg="false";
	     }
       
	     return flg;	
	}
	
	static String randomString(){
		   StringBuilder sb = new StringBuilder( 8 );
		   for( int i = 0; i < 8; i++ ) 
		      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		   return sb.toString();
		}
}
