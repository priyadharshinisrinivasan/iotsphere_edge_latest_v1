package com.ey.iot.iotsphereeg.websocket;

import org.apache.log4j.Logger;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import org.springframework.web.socket.sockjs.frame.Jackson2SockJsMessageCodec;

import com.ey.iot.iotsphereeg.utility.AppProperty;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class WSClient {
	private StompSession stompSession;
	private Logger logger = Logger.getLogger(this.getClass());

	private final static WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

	public ListenableFuture<StompSession> connect() {

		Transport webSocketTransport = new WebSocketTransport(new StandardWebSocketClient());
		List<Transport> transports = Collections.singletonList(webSocketTransport);

		SockJsClient sockJsClient = new SockJsClient(transports);
		sockJsClient.setMessageCodec(new Jackson2SockJsMessageCodec());

		WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);

		String url = (new AppProperty()).getPropertyValue("websocket_uri");
		return stompClient.connect(url, headers, new MyHandler());
	}

	public void subscribeGreetings(StompSession stompSession) {
		stompSession.subscribe("/channel/public", new StompFrameHandler() {

			public Type getPayloadType(StompHeaders stompHeaders) {
				return byte[].class;
			}

			public void handleFrame(StompHeaders stompHeaders, Object o) {
				// logger.info("Received greeting " + new String((byte[]) o));
			}
		});
	}

	public void sendHello(StompSession stompSession, String cnt) {
		String jsonHello = "{ \"sender\" : \"EdgeGateway\",\"type\" : \"CHAT\",\"content\" : " + cnt + " }";
		stompSession.send("/app/chat.sendMessage", jsonHello.getBytes());
	}

	private class MyHandler extends StompSessionHandlerAdapter {
		public void afterConnected(StompSession stompSession, StompHeaders stompHeaders) {
			// logger.info("Now connected");
			String jsonHello = "{ \"sender\" : \"EdgeGateway\",\"content\":{},\"type\" : \"JOIN\" }";
			stompSession.send("/app/chat.addDevice", jsonHello.getBytes());
		}
	}

	public WSClient() {
		while (true) {
			try {
				ListenableFuture<StompSession> f = connect();
				setStompSession(f.get());
				break;
			} catch (ExecutionException | InterruptedException e) {
				logger.error("Error while connecting... Trying again!!!");
			}
		}
		// logger.info("Subscribing to greeting topic using session " +
		// getStompSession().getSessionId());
		subscribeGreetings(getStompSession());
	}

	public StompSession getStompSession() {
		return stompSession;
	}

	public void setStompSession(StompSession stompSession) {
		this.stompSession = stompSession;
	}

}
