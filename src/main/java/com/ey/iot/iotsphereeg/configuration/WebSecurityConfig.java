package com.ey.iot.iotsphereeg.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.ey.iot.iotsphereeg.configuration.CustomAuthenticationProvider;

	@Configuration
	@EnableWebSecurity
	public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
		@Autowired
		private CustomAuthenticationProvider authProvider;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
			    .antMatchers("/ws/**","/ws","/login","/register","/reset","/logout","/jwt/**","/").permitAll()
			    .antMatchers("/dashboard**").hasAnyRole("ROLE_USER","USER")
			    .antMatchers("/admin").hasAnyRole("ROLE_ADMIN","ADMIN")
					.anyRequest().authenticated().and().formLogin().loginPage("/login")
					.defaultSuccessUrl("/dashboard").failureUrl("/loginError")
					// .successHandler(yourSuccessHandlerBean)
					.permitAll().and().logout().permitAll()			    
					.and().httpBasic().realmName("REALM").authenticationEntryPoint(getBasicAuthEntryPoint());
			http.csrf().disable();
		}


	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}
	
	  @Bean
	    public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint(){
	        return new CustomBasicAuthenticationEntryPoint();
	    }
	
}