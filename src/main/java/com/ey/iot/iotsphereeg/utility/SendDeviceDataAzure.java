package com.ey.iot.iotsphereeg.utility;

import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;

public class SendDeviceDataAzure {
	private String IoTHubHostName;
	private String connectionStringIoTHub;
	public SendDeviceDataAzure()
	{
		IoTHubHostName = (new AppProperty()).getPropertyValue("IoTHubHostName");
		connectionStringIoTHub = (new AppProperty()).getPropertyValue("connectionStringIoTHub");
		//System.out.println("Service sendDeviceDataAzure created");
	}
	Map<String, MessageSender_Azure> message = new HashMap<String, MessageSender_Azure>();
	void setup(String DeviceId) {
		MessageSender_Azure messageSender = new MessageSender_Azure(DeviceId, connectionStringIoTHub, IoTHubHostName);
		message.put(DeviceId, messageSender);
	}

	public void sendMessage(JSONObject telemetryDataPoint) {
		if (!message.containsKey(telemetryDataPoint.get("DeviceId").toString()))
			setup((telemetryDataPoint.get("DeviceId")).toString());
		MessageSender_Azure messageSender = message.get((telemetryDataPoint.get("DeviceId")).toString());
		messageSender.setTelemetryDataPoint(telemetryDataPoint);
		Thread t = new Thread(messageSender);
		t.start();
		//message.get(telemetryDataPoint.getDeviceId()).send(telemetryDataPoint);;
		//messageSender_Azure.send(telemetryDataPoint);
	}

	public void close(String string) {
		if (message.containsKey(string))
			message.get(string).close();

	}
}