package com.ey.iot.iotsphereeg.utility;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Resource;
import javax.json.Json;
import javax.json.JsonObject;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.ey.iot.iotsphereeg.controller.EdgeController;
import com.ey.iot.iotsphereeg.events.NetworkExceptionEvent;
import com.ey.iot.iotsphereeg.model.Calibration_Config;
import com.ey.iot.iotsphereeg.service.Calibration_ConfigService;
import com.ey.iot.iotsphereeg.service.PortConnectionDetailsService;
import com.ey.iot.iotsphereeg.websocket.WSClient;
import com.google.gson.Gson;
import com.microsoft.azure.sdk.iot.device.Message;

import gnu.io.*;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import java.util.Properties;

@Service()
@Component
public class Main_Edge implements SerialPortEventListener, Runnable {

	Producer<Long, String> producer=null;
	@Autowired
	EdgeController edgecontroller;
	
	@Resource
	@Value("${prop.KAFKA_ENABLED}")
	public String KAFKA_ENABLED_VAL;
	
	@Resource
	@Value("${prop.KAFKA_TOPIC}")
	public String KAFKA_TOPIC_VAL;
	
	@Resource
	@Value("${prop.KAFKA_BOOTSTRAP_SERVERS}")
	public String KAFKA_BOOTSTRAP_SERVERS_VAL;

	@Autowired
	DeviceManagement dm;
	
	@Autowired
	PortConnectionDetailsService portConnectionDetailsService;
	
	@Autowired
	Calibration_ConfigService calibration_ConfigService;
	
	@Autowired
    private ApplicationEventPublisher applicationEventPublisher;
	
	JSONObject _JSONpayload;
	
	public WSClient ws = null;
	Logger logger = Logger.getLogger(this.getClass());
	SendDeviceDataAzure sendDeviceDataAzure = new SendDeviceDataAzure();
	public Thread thread;
	public String port;
	public String selectedDeviceId;
	public String loginId;
	public boolean stopThread = false;

	CommPortIdentifier _portId;
	SerialPort _serialPort1;
	InputStream _inputStream;
	int _cnt;
	static String option;

	public static String showport() {
		Enumeration<?> _portList = CommPortIdentifier.getPortIdentifiers();
		CommPortIdentifier _portId = null;
		String port = "";
		while (_portList.hasMoreElements()) {
			_portId = (CommPortIdentifier) _portList.nextElement();
			if (_portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				//System.out.println((_portId.getName()));
				port = port + _portId.getName() + ',';
			}
		}
		port = port + "TestPort";
		return port;
	}

	@SuppressWarnings("unchecked")
	public void connectport(String port,String selectedDeviceId) throws Exception {
		if(KAFKA_ENABLED_VAL.equalsIgnoreCase("Y"))
		{		
		producer = createProducer();
		}
		if (port.equals("TestPort")) {
			
			while (!stopThread) {			
				try {
				//Smart warehouse
					Calendar _cal = Calendar.getInstance();
					_cal.add(Calendar.MONTH, -1);
					SimpleDateFormat _sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					_JSONpayload = new JSONObject();
					_JSONpayload.put("date", _sdf.format(_cal.getTime()));
					_JSONpayload.put("DeviceId", selectedDeviceId);			
					_JSONpayload.put("FireAlert", (int) (Math.random() * 2));
					_JSONpayload.put("SmokeAlert", (int) (Math.random() * 2));
					_JSONpayload.put("GeneralFault", (int) (Math.random() * 2));
					_JSONpayload.put("Sprinkler", (int) (Math.random() * 2));
					_JSONpayload.put("ManualTest", (int) (Math.random() * 2));
					_JSONpayload.put("FireDoor", (int) (Math.random() * 2));
					_JSONpayload.put("Auxiliary", (int) (Math.random() * 2));
					_JSONpayload.put("Alarm", (int) (Math.random() * 2));
					_JSONpayload.put("Pump", (int) (Math.random() * 2));
					_JSONpayload.put("Mains", (int) (Math.random() * 2));
					_JSONpayload.put("GasLeakageAlert", (int) (Math.random() * 2));
					_JSONpayload.put("BatteryCondition", "On");
					_JSONpayload.put("FuelLevel", (int) (Math.random() * 100));
					_JSONpayload.put("EngineRunning", (int) (Math.random() * 2));
					_JSONpayload.put("Volt1", (int) (200 + (int)(Math.random() * ((250 - 200) + 1))));
					_JSONpayload.put("Volt2", (int) (200 + (int)(Math.random() * ((250 - 200) + 1))));
					_JSONpayload.put("Volt3", (int) (200 + (int)(Math.random() * ((250 - 200) + 1))));
					_JSONpayload.put("KWH1", (int) (Math.random() * 100) / 10);
					_JSONpayload.put("KWH2", (int) (Math.random() * 100) / 10);
					_JSONpayload.put("KWH3", (int) (Math.random() * 100) / 10);
					_JSONpayload.put("OilPressure", (int) (Math.random() * 100) / 10);
					_JSONpayload.put("Pressure", (int) (Math.random() * 100) / 10);
					_JSONpayload.put("WaterTemperature", (int) (10 + (int)(Math.random() * ((50 - 10) + 1))));
					_JSONpayload.put("Temperature", (int) (10 + (int)(Math.random() * ((50 - 10) + 1))));
					_JSONpayload.put("Humidity", (int) (10 + (int)(Math.random() * ((50 - 10) + 1))));
					_JSONpayload.put("DewPoint", (int) (10 + (int)(Math.random() * ((50 - 10) + 1))));
					_JSONpayload.put("DewPointFast", (int) (10 + (int)(Math.random() * ((50 - 10) + 1))));
					_JSONpayload.put("Sonardistance", (int) (10 + (int)(Math.random() * ((50 - 10) + 1))));
					_JSONpayload.put("CCTVStatus", (int) (Math.random() * 100) / 10);
					_JSONpayload.put("SensorStatus", (int) (Math.random() * 100) / 10);
					_JSONpayload.put("Zone", (int) (1 + (int)(Math.random() * ((4 - 1) + 1))));
					_JSONpayload.put("DateTime", _sdf.format(_cal.getTime()));
									
					if (ws == null)
						ws = new WSClient();
					if (!(ws.getStompSession()).isConnected())
						ws = new WSClient();
					Calibration_Config cc = calibration_ConfigService
							.getByDeviceId(_JSONpayload.get("DeviceId").toString());
					JSONObject oldcal;
					try {
						if (cc.getProperty() == null)
							oldcal = new JSONObject();
						else
							oldcal = (JSONObject) new JSONParser().parse(cc.getProperty());
					} catch (ParseException e) {
						oldcal = new JSONObject();
					}
					Set<?> i = oldcal.keySet();
					Iterator<?> keys = i.iterator();
					// Iterator<?> keys = (Iterator<?>) oldcal.keySet();
					JSONObject newdata;
					newdata = _JSONpayload;
					while (keys.hasNext()) {
						String key = (String) keys.next();
						if (newdata.containsKey(key)) {
							float temp_old = Float.parseFloat(oldcal.get(key).toString());
							float temp_new = Float.parseFloat(newdata.get(key).toString());
							float temp_update = temp_old + temp_new;
							newdata.put(key, "" + temp_update);
						}
					}
					ws.sendHello(ws.getStompSession(), newdata.toJSONString());
					if(KAFKA_ENABLED_VAL.equalsIgnoreCase("Y"))
					{
					kafkasend(newdata.toString(),producer);
					}
					if (dm.isDeviceRegisted(newdata.get("DeviceId").toString()))
						getSendDeviceDataAzure().sendMessage(newdata);
					Thread.sleep(3000);
					
		if(selectedDeviceId.equalsIgnoreCase("IoT_02"))//asset tracking
		{
			Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				JSONObject JSONpayload = new JSONObject();
				JSONpayload.put("date", sdf.format(cal.getTime()));
				//JSONpayload.put("temp1", "28");
				JSONpayload.put("temp1", (int) (Math.random() * 100));
				JSONpayload.put("humid1", "65");
				JSONpayload.put("volt1", (int) (Math.random() * 100) / 10);
				JSONpayload.put("level1", (int) (Math.random() * 100) / 10);//x axis
				JSONpayload.put("device", "101");
				JSONpayload.put("DeviceId", selectedDeviceId);
				JSONpayload.put("lat1", "9.580520");
				JSONpayload.put("long1", "79.878593");
				JSONpayload.put("title1", "J01");
				JSONpayload.put("desc1", "J01desc1");
				JSONpayload.put("destcnt", "6");
				getSendDeviceDataAzure().sendMessage(JSONpayload);			
				Thread.sleep(3000);
			
		}
					/*_JSONpayload = new JSONObject();
							//(JSONObject) parser.parse("{\"source\": \"Cumulocity\",\"time\": \"2018-10-01T06:45:49.94+00:00\",\"msgid\": \"172145\",\"deviceid\": \"427\"\"devicetype\": \"c8ydemoAndroid\",\"SignalStrength\": {\"rssi\": null },\"RssiUnit\": \"\",\"RssiValue\": \"\",\"AccelerationYUnit\": \"G\",\"AccelerationYValue\": -0.0068359375,\"AccelerationXUnit\": \"G\",\"AccelerationXValue\": 0.04052734375,\"AccelerationZUnit\": \"G\",\"AccelerationZValue\": 0.975341796875,\"air_pressure\": 10.1781127929688,\"GyroscopeXUnit\": \"?/s\",\"GyroscopeXValue\": -0.00004276056642993353,\"GyroscopeYUnit\": \"?/s\",\"GyroscopeYValue\": -0.00041477748891338706,\"GyroscopeZUnit\": \"?/s\",\"GyroscopeZValue\": -0.0014404202811419964,\"LuxometerUnit\": \"lux\",\"LuxometerValue\": 0.0,\"CompassXUnit\": \"uT\",\"CompassXValue\": -14.699999809265137,\"CompassYUnit\": \"uT\",\"CompassYValue\": -29.579999923706055,\"CompassZUnit\": \"uT\",\"CompassZValue\": -1.7400000095367432,\"SensorTagIRTempUnit\": \"?C\",\"SensorTagIRTempValue\": 26.125,\"SensorTagAmbientTempUnit\": \"?C\",\"SensorTagAmbientTempValue\": 29.375,\"SensorTagHumidityUnit\": \"%rH\",\"SensorTagHumidityValue\": 42.609291076660156,\"SensorTagLuxometerUnit\": \"lux\",\"SensorTagLuxometerValue\": 2.34,\"SensorTagBarometerUnit\": \"mBar\",\"SensorTagBarometerValue\": 927.6416,\"latitude\": -37.8110743,\"longitude\": 144.9601583,\"snr\": \" \",\"station\": \" \",\"data\": \" \",\"avgSnr\": \" \",\"HI1_Time\": \"2018-10-01T06:45:49.94+00:00\",\"HI2_Time\": \"2018-10-01T06:45:49.94+00:00\",\"IngestTime\": \"2018-09-27T06:45:49.94+00:00\",\"alertSensorData\": [{\"inputVariable\": \"deviceLuxometer\",\"value\": 0.0 },{\"inputVariable\": \"sensorLuxometer\",\"value\": 2.34 },{\"inputVariable\": \"sensorTemperature\",\"value\": 26.125},{\"inputVariable\": \"SensorAmbientTemperature\",\"value\": 29.375},{\"inputVariable\": \"sensorHumidity\",\"value\": 42.609291076660156}]}");
					_JSONpayload.put("source", "Cumulocity");
					_JSONpayload.put("time", "2018-10-01T06:45:49.94+00:00");
					_JSONpayload.put("msgid", "172145");
					_JSONpayload.put("deviceid", "427");
					_JSONpayload.put("DeviceId", "427");
					_JSONpayload.put("devicetype", "c8ydemoAndroid");
					
					_JSONpayload.put("RssiUnit", "");
					_JSONpayload.put("RssiValue", "");
					_JSONpayload.put("AccelerationYUnit", "G");
					_JSONpayload.put("AccelerationYValue", -0.0068359375);
					_JSONpayload.put("AccelerationXUnit", "G");
					_JSONpayload.put("AccelerationXValue", 0.0483593759);
					_JSONpayload.put("AccelerationZUnit", "G");
					_JSONpayload.put("AccelerationZValue", 0.9068359375);
					_JSONpayload.put("air_pressure", 10.1781127929688);
					_JSONpayload.put("GyroscopeXUnit", "?/s");
					_JSONpayload.put("GyroscopeXValue", -0.00004276056642993353);
					_JSONpayload.put("GyroscopeYUnit", "?/s");
					_JSONpayload.put("GyroscopeYValue", -0.00004276056642993353);
					_JSONpayload.put("GyroscopeZUnit", "?/s");
					_JSONpayload.put("GyroscopeZValue", -0.00004276056642993353);
					_JSONpayload.put("LuxometerUnit", "lux");
					_JSONpayload.put("LuxometerValue", 0.0);
					_JSONpayload.put("CompassXUnit", "uT");
					_JSONpayload.put("CompassXValue", -14.699999809265137);
					_JSONpayload.put("CompassYUnit", "uT");
					_JSONpayload.put("CompassYValue", -14.699999809265137);
					_JSONpayload.put("CompassZUnit", "uT");
					_JSONpayload.put("CompassZValue", -14.699999809265137);
					_JSONpayload.put("SensorTagIRTempUnit", "?C");
					_JSONpayload.put("SensorTagIRTempValue", 26.125);
					_JSONpayload.put("SensorTagAmbientTempUnit", "?C");
					_JSONpayload.put("SensorTagAmbientTempValue", 29.125);
					_JSONpayload.put("SensorTagHumidityUnit", "%rH");
					_JSONpayload.put("SensorTagHumidityValue", 42.609291076660156);
					_JSONpayload.put("SensorTagLuxometerUnit", "lux");
					_JSONpayload.put("SensorTagLuxometerValue", 2.34);
					_JSONpayload.put("SensorTagBarometerUnit", "mBar");
					_JSONpayload.put("SensorTagBarometerValue", 927.6416);
					_JSONpayload.put("latitude", -37.8110743);
					_JSONpayload.put("longitude", 144.9601583);
					_JSONpayload.put("snr", "");
					_JSONpayload.put("station", "");
					_JSONpayload.put("data", "");
					_JSONpayload.put("avgSnr", "");
					_JSONpayload.put("HI1_Time", "2018-10-01T06:45:49.94+00:00");
					_JSONpayload.put("HI2_Time", "2018-10-01T06:45:49.94+00:00");
					_JSONpayload.put("IngestTime","2018-10-01T06:45:49.94+00:00");
				
					
					JSONArray array = new JSONArray();
					JSONObject arrayElementOne = new JSONObject();
					arrayElementOne.put("rssi", "null");
					array.add(arrayElementOne);
					_JSONpayload.put("SignalStrength", array);
					
								
					
					JSONObject arrayElementOneArrayElementOne = new JSONObject();
					arrayElementOneArrayElementOne.put("inputVariable", "deviceLuxometer");
					arrayElementOneArrayElementOne.put("value", 0.0);

					JSONObject arrayElementOneArrayElementTwo = new JSONObject();
					arrayElementOneArrayElementTwo.put("inputVariable", "sensorLuxometer");
					arrayElementOneArrayElementTwo.put("value", 0.0);
					
					JSONObject arrayElementOneArrayElementThree = new JSONObject();
					arrayElementOneArrayElementThree.put("inputVariable", "sensorTemperature");
					arrayElementOneArrayElementThree.put("value", 26.125);
					
					JSONObject arrayElementOneArrayElementFour = new JSONObject();
					arrayElementOneArrayElementFour.put("inputVariable", "SensorAmbientTemperature");
					arrayElementOneArrayElementFour.put("value", 29.375);
					
					JSONObject arrayElementOneArrayElementFive = new JSONObject();
					arrayElementOneArrayElementFive.put("inputVariable", "sensorHumidity");
					arrayElementOneArrayElementFive.put("value", 42.609291076660156);
					
					      
					JSONArray arrayElementOneArray = new JSONArray();

					arrayElementOneArray.add(arrayElementOneArrayElementOne);
					arrayElementOneArray.add(arrayElementOneArrayElementTwo);
					arrayElementOneArray.add(arrayElementOneArrayElementThree);
					arrayElementOneArray.add(arrayElementOneArrayElementFour);
					arrayElementOneArray.add(arrayElementOneArrayElementFive);
					_JSONpayload.put("alertSensorData", arrayElementOneArray);
					
				    getSendDeviceDataAzure().sendMessage(_JSONpayload);			
					Thread.sleep(3000);*/
					
				}
				catch(Exception _e) {
					_e.printStackTrace();
					
					NetworkExceptionEvent networkExceptionEvent = new NetworkExceptionEvent(this,"IOT hub not reachable");
					applicationEventPublisher.publishEvent(networkExceptionEvent);
				}		
			
			}
			getSendDeviceDataAzure().close(selectedDeviceId);
		} else// selects actual com port
		{
			Enumeration<?> _portList2 = CommPortIdentifier.getPortIdentifiers();
			while (_portList2.hasMoreElements()) {
				_portId = (CommPortIdentifier) _portList2.nextElement();
				if (_portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					//System.out.println((_portId.getName()));
					if (_portId.getName().equals(port)) {
						if ( _portId.isCurrentlyOwned() )
							 {
							 System.out.println("Error: Port is currently in use");
							}
						else
						{
						//System.out.println("Executing port: " + _portId.getName());
						_serialPort1 = (SerialPort) _portId.open("USBCommunicator", 2000);
						//System.out.println(_portId.getName() + " opened");
						_inputStream = _serialPort1.getInputStream();
						_serialPort1.addEventListener(this);
						_serialPort1.notifyOnBreakInterrupt(true);
						_serialPort1.notifyOnDataAvailable(true);
						_serialPort1.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
								SerialPort.PARITY_NONE);
						_serialPort1.setDTR(false);
						_serialPort1.setRTS(false);
						}
					}
				}
			}
		}
	}

	public void serialEvent(SerialPortEvent _event) {
		//System.out.println("*******************Serial event called************************");
		switch (_event.getEventType()) {
		case SerialPortEvent.BI:
			System.out.println("Break interrupt");
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
			System.out.println("CTS");
		case SerialPortEvent.DSR:
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:

			break;
		case SerialPortEvent.DATA_AVAILABLE:
			StringBuffer _readBuffer = new StringBuffer();
			int _c;
			try {
				while ((_c = _inputStream.read()) != 10) {
					if (_c != 13)
						_readBuffer.append((char) _c);
				}
				String _scannedInput = _readBuffer.toString();
				sendData(_scannedInput);

			} 

			catch (IOException _e) {
			disconnect();
		 	edgecontroller.disconnect(port, selectedDeviceId, null);
		 	logger.info(_e);
		} catch (ClassNotFoundException e) {
				e.printStackTrace();
		} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	void sendData(String _data) throws IOException, ClassNotFoundException, SQLException {
  if(_data.contains(","))
  {	  
		String _dataarray[] = _data.split(",");
		Calendar _cal = Calendar.getInstance();
		SimpleDateFormat _sdf = new SimpleDateFormat("HH:mm:ss");
		String devid = "IoT_";
		if (Integer.parseInt(_dataarray[0]) < 10)
			devid += '0' + _dataarray[0];
		else
			devid += _dataarray[0];
//		if(devid=="IoT_01")//other use cases
//		{
//		_JSONpayload = new JSONObject();
//		_JSONpayload.put("date", _sdf.format(_cal.getTime()));
//		_JSONpayload.put("temp1", _dataarray[1]);
//		_JSONpayload.put("volt1", _dataarray[2]);
//		_JSONpayload.put("humid1", _dataarray[3]);
//		_JSONpayload.put("lat1", "9.580520");
//		_JSONpayload.put("long1", "79.878593");
//		_JSONpayload.put("title1", "IoT_01");
//		_JSONpayload.put("desc1", "IoT_01desc1");
//		_JSONpayload.put("destcnt", _dataarray[4]);
//		_JSONpayload.put("level1", _dataarray[5]);
//		_JSONpayload.put("DeviceId", devid);
//		}
		
		//sample- 5,0,1,1,1,0,1,1,0,1,1,2,ON,50,1,230,240,235,1000,125,345,20,40,48,28,80,200,0,1,2
		//Smart warehouse
		
			_JSONpayload = new JSONObject();
	
			_JSONpayload.put("DeviceId", devid);
			if(devid.equalsIgnoreCase("5")||devid.equalsIgnoreCase("IoT_05")||devid.equalsIgnoreCase("05"))
			{
			_JSONpayload.put("FireAlert", _dataarray[1]);
			_JSONpayload.put("SmokeAlert", _dataarray[2]);
			_JSONpayload.put("GeneralFault",_dataarray[3]);
			_JSONpayload.put("Sprinkler", _dataarray[4]);
			_JSONpayload.put("ManualTest", _dataarray[5]);
			_JSONpayload.put("FireDoor",_dataarray[6]);
			_JSONpayload.put("Auxiliary", _dataarray[7]);
			_JSONpayload.put("Alarm", _dataarray[8]);
			_JSONpayload.put("Pump", _dataarray[9]);
			_JSONpayload.put("Mains", _dataarray[10]);
			_JSONpayload.put("GasLeakageAlert", _dataarray[11]);
			_JSONpayload.put("BatteryCondition",_dataarray[12]);
			_JSONpayload.put("FuelLevel",_dataarray[13]);
			_JSONpayload.put("EngineRunning", _dataarray[14]);
			_JSONpayload.put("Volt1", _dataarray[15]);
			_JSONpayload.put("Volt2",_dataarray[16]);
			_JSONpayload.put("Volt3", _dataarray[17]);
			_JSONpayload.put("KWH1", _dataarray[18]);
			_JSONpayload.put("KWH2", _dataarray[19]);
			_JSONpayload.put("KWH3", _dataarray[20]);
			_JSONpayload.put("OilPressure", _dataarray[21]);
			_JSONpayload.put("Pressure", _dataarray[22]);
			_JSONpayload.put("WaterTemperature", _dataarray[23]);
			_JSONpayload.put("Temperature", _dataarray[24]);
			_JSONpayload.put("Humidity", _dataarray[25]);
			_JSONpayload.put("DewPoint", _dataarray[26]);
			_JSONpayload.put("DewPointFast", _dataarray[27]);
			_JSONpayload.put("Sonardistance", _dataarray[28]);
			_JSONpayload.put("CCTVStatus", _dataarray[29]);
			_JSONpayload.put("SensorStatus", _dataarray[30]);
			_JSONpayload.put("Zone",_dataarray[31]);
			_JSONpayload.put("DateTime", _sdf.format(_cal.getTime()));
			_JSONpayload.put("date", _sdf.format(_cal.getTime()));
			}
			else if(devid.equalsIgnoreCase("1")||devid.equalsIgnoreCase("IoT_01")||devid.equalsIgnoreCase("01")||devid.equalsIgnoreCase("2")||devid.equalsIgnoreCase("IoT_02")||devid.equalsIgnoreCase("02"))//old device with accel //2,0,8.43,0,0,116,93,6,0|   devid,tempertre,volt,humdty,reset,x,y,z,water
			{
				_JSONpayload.put("Volt1", _dataarray[2]);
				_JSONpayload.put("X", _dataarray[5]);
				_JSONpayload.put("Y",_dataarray[6]);
				_JSONpayload.put("Z", _dataarray[7]);
				_dataarray[8]=_dataarray[8].replaceAll("\\|", "");
				_JSONpayload.put("Sprinkler", _dataarray[8]);//waterleakage
				_JSONpayload.put("Zone", "1");
			}
		
		if (!stopThread) {
			// getWebSocketSession().sendMessage(new
			// TextMessage(telemetryDataPoint.serialize()));
			if (ws == null)
				ws = new WSClient();
			if (!(ws.getStompSession()).isConnected())
				ws = new WSClient();
			Calibration_Config cc = calibration_ConfigService
					.getByDeviceId(_JSONpayload.get("DeviceId").toString());
			JSONObject oldcal;
			try {
				if (cc.getProperty() == null)
					oldcal = new JSONObject();
				else
					oldcal = (JSONObject) new JSONParser().parse(cc.getProperty());
			} catch (ParseException e) {
				oldcal = new JSONObject();
			}
			Set<?> i = oldcal.keySet();
			Iterator<?> keys = i.iterator();
			// Iterator<?> keys = (Iterator<?>) oldcal.keySet();
			JSONObject newdata;
			newdata = _JSONpayload;
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (newdata.containsKey(key)) {
					float temp_old = Float.parseFloat(oldcal.get(key).toString());
					float temp_new = Float.parseFloat(newdata.get(key).toString());
					float temp_update = temp_old + temp_new;
					newdata.put(key, "" + temp_update);
				}
			}
			ws.sendHello(ws.getStompSession(), newdata.toJSONString());
			if(KAFKA_ENABLED_VAL.equalsIgnoreCase("Y"))
			{
			kafkasend(newdata.toString(),producer);
			}
			if (dm.isDeviceRegisted(newdata.get("DeviceId").toString()))
				getSendDeviceDataAzure().sendMessage(newdata);
		} else {
			getSendDeviceDataAzure().close(_JSONpayload.get("DeviceId").toString());
			disconnect();
		}
	}
	}

	public void disconnect() {
		if (_serialPort1 != null) {
			try {
				//System.out.println("Disconnecting....");
				_inputStream.close();
			} catch (IOException ex) {
				//System.out.println("Error!!!!");
			}
			_serialPort1.close();
			if(producer!=null)
			{
			producer.flush();
	       	producer.close();
			}
		}
	}

	@Override
	public void run() {
		try {
			connectport(port,selectedDeviceId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SendDeviceDataAzure getSendDeviceDataAzure() {
		return sendDeviceDataAzure;
	}

	public void setSendDeviceDataAzure(SendDeviceDataAzure sendDeviceDataAzure) {
		this.sendDeviceDataAzure = sendDeviceDataAzure;
	}
	
	private  Producer<Long, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
        		KAFKA_BOOTSTRAP_SERVERS_VAL);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaIoTSphereG01Producer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                                        LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                                    StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }
	 public void kafkasend(String jsondata,Producer<Long, String> producer)
	 {
	
		  long time = System.currentTimeMillis();
		try
		{
		
		final ProducerRecord<Long, String> record =
               new ProducerRecord<>(KAFKA_TOPIC_VAL, jsondata);
       producer.send(record, (metadata, exception) -> {
           long elapsedTime = System.currentTimeMillis() - time;
           if (metadata != null) {
               System.out.printf("sent record(key=%s value=%s) " +
                               "meta(partition=%d, offset=%d) time=%d\n",
                       record.key(), record.value(), metadata.partition(),
                       metadata.offset(), elapsedTime);
           } else {
               exception.printStackTrace();
           }
       });
		}
       finally
       {
       	
       }
	 }
}
