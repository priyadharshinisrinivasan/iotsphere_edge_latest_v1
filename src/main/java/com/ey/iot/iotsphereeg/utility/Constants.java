package com.ey.iot.iotsphereeg.utility;

public class Constants {

	public static final String CONNECTED_STATUS = "Connected";
	public static final String DISCONNECTED_STATUS = "Disconnected";
	public static final String NOT_RUNNING = "Already Not Running";
	
}
