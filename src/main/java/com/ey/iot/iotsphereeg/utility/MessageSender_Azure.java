package com.ey.iot.iotsphereeg.utility;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.JsonSyntaxException;
import com.microsoft.azure.sdk.iot.device.DeviceClient;
import com.microsoft.azure.sdk.iot.device.IotHubClientProtocol;
import com.microsoft.azure.sdk.iot.device.IotHubEventCallback;
import com.microsoft.azure.sdk.iot.device.IotHubStatusCode;
import com.microsoft.azure.sdk.iot.device.Message;
import com.microsoft.azure.sdk.iot.service.Device;
import com.microsoft.azure.sdk.iot.service.RegistryManager;
import com.microsoft.azure.sdk.iot.service.exceptions.IotHubException;

public class MessageSender_Azure implements Runnable {
	Logger logger = Logger.getLogger(this.getClass());
	private String IoTHubHostName;
	private String connectionStringIoTHub;
	private static IotHubClientProtocol protocol = IotHubClientProtocol.MQTT;
	public volatile boolean stopThread = false;
	protected String DEVICEID;
	public String connString;
	public Thread thread;
	protected DeviceClient client;
	private JSONObject telemetryDataPoint;
	private boolean clientClosed;

	public MessageSender_Azure(String DEVICEID, String connectionStringIoTHub, String IoTHubHostName) {
		this.DEVICEID = DEVICEID;
		this.setIoTHubHostName(IoTHubHostName);
		this.setConnectionStringIoTHub(connectionStringIoTHub);
		try {
			RegistryManager registryManager = RegistryManager.createFromConnectionString(connectionStringIoTHub);
			
			/*Device device;
			DEVICEID="IOT_01";
			device = registryManager.getDevice(DEVICEID);
			String key = device.getPrimaryKey();
			key="8XiujIovS+a0Zo1fGxJ9NA==";
			connString = IoTHubHostName + ";DeviceId=" + DEVICEID + ";SharedAccessKey=" + key + ";";
			client = new DeviceClient("HostName=IoTSphereHub.azure-devices.net;DeviceId=IOT_01;SharedAccessKey=8XiujIovS+a0Zo1fGxJ9NA==", protocol);*/
			
			Device device;
			device = registryManager.getDevice(DEVICEID);
			String key = device.getPrimaryKey();
			
			String[] connectionStringProperties = connectionStringIoTHub.split(";");
			String hostName = connectionStringProperties[0];//HostName=iotsphereclone.azure-devices.net
			
			client = new DeviceClient(hostName+";DeviceId="+DEVICEID+";SharedAccessKey=" + key + "", protocol);
			
			client.open();
			this.setClientClosed(false);
		} catch (Exception e1) {
			
			logger.error("Error in azure iothub connection" + e1);
			connString = null;
		}
	}
	

	void send() {
		String msgStr = getTelemetryDataPoint().toJSONString();
		Message msg = new Message(msgStr);
		//System.out.println(msgStr);
		Object lockobj = new Object();
		EventCallback callback = new EventCallback();
		client.sendEventAsync(msg, callback, lockobj);
		synchronized (lockobj) {
			try {
				lockobj.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	void read() {
	}

	public void close() {
		try {
			client.closeNow();
			this.setClientClosed(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getIoTHubHostName() {
		return IoTHubHostName;
	}

	public void setIoTHubHostName(String ioTHubHostName) {
		IoTHubHostName = ioTHubHostName;
	}

	public String getConnectionStringIoTHub() {
		return connectionStringIoTHub;
	}

	public void setConnectionStringIoTHub(String connectionStringIoTHub) {
		this.connectionStringIoTHub = connectionStringIoTHub;
	}

	public JSONObject getTelemetryDataPoint() {
		return telemetryDataPoint;
	}

	public void setTelemetryDataPoint(JSONObject telemetryDataPoint) {
		this.telemetryDataPoint = telemetryDataPoint;
	}

	@Override
	public void run() { 
		if(this.isClientClosed())
			try {
				client.open();
			} catch (IOException e) {
				e.printStackTrace();
			}
		send();
	}

	public boolean isClientClosed() {
		return clientClosed;
	}

	public void setClientClosed(boolean clientClosed) {
		this.clientClosed = clientClosed;
	}
}

class EventCallback implements IotHubEventCallback {
	@Override
	public void execute(IotHubStatusCode statusCode, Object context) {
		//System.out.println("IoT Hub responded to message with status " + statusCode.name());

		if (context != null) {
			synchronized (context) {
				context.notify();
			}
		}
	}
}