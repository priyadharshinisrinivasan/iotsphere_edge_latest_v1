package com.ey.iot.iotsphereeg.utility;

import java.io.IOException;
import java.util.Random;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import com.google.gson.JsonSyntaxException;
import com.microsoft.azure.sdk.iot.device.DeviceClient;
import com.microsoft.azure.sdk.iot.device.IotHubClientProtocol;
import com.microsoft.azure.sdk.iot.device.IotHubEventCallback;
import com.microsoft.azure.sdk.iot.device.IotHubStatusCode;
import com.microsoft.azure.sdk.iot.device.Message;
import com.microsoft.azure.sdk.iot.service.Device;
import com.microsoft.azure.sdk.iot.service.RegistryManager;
import com.microsoft.azure.sdk.iot.service.exceptions.IotHubException;

public class MessageSender implements Runnable {
	Logger logger = Logger.getLogger(this.getClass());
	private String IoTHubHostName;
	private String connectionStringIoTHub;
	private static IotHubClientProtocol protocol = IotHubClientProtocol.MQTT;
	public volatile boolean stopThread = false;
	protected String DEVICEID;
	public String connString;
	public Thread thread;

	public MessageSender(String DEVICEID, String connectionStringIoTHub, String IoTHubHostName) {
		this.setIoTHubHostName(IoTHubHostName);
		this.setConnectionStringIoTHub(connectionStringIoTHub);
		RegistryManager registryManager;
		try {
			registryManager = RegistryManager.createFromConnectionString(connectionStringIoTHub);
			Device device;
			device = registryManager.getDevice(DEVICEID);
			String key = device.getPrimaryKey();
			connString = IoTHubHostName + ";DeviceId=" + DEVICEID + ";SharedAccessKey=" + key + ";";

		} catch (JsonSyntaxException | IotHubException | IOException e1) {
			connString = null;
			logger.error("Error in azure iothub connection" + e1);
		}
		this.DEVICEID = DEVICEID;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public void run() {
		try {
			double avgTempSpeed = 40;
			double avgHumidSpeed = 90;
			Random rand = new Random();
			DeviceClient client = new DeviceClient(connString, protocol);
			client.open();

			while (!stopThread) {
				double currentTempSpeed = avgTempSpeed + rand.nextDouble() * 4 - 2;
				double currentHumidSpeed = avgHumidSpeed + rand.nextDouble() * 4 - 2;
				JSONObject telemetryDataPoint = new JSONObject();

				telemetryDataPoint.put("DeviceId",DEVICEID);
				telemetryDataPoint.put("Temperature",currentTempSpeed);
				telemetryDataPoint.put("Humidity",currentHumidSpeed);

				String msgStr = telemetryDataPoint.toJSONString();
				Message msg = new Message(msgStr);

				//System.out.println(msgStr);

				Object lockobj = new Object();
				EventCallback callback = new EventCallback();

				client.sendEventAsync(msg, callback, lockobj);

				synchronized (lockobj) {
					lockobj.wait();
				}

				Thread.sleep(1000);
			}
			client.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getIoTHubHostName() {
		return IoTHubHostName;
	}

	public void setIoTHubHostName(String ioTHubHostName) {
		IoTHubHostName = ioTHubHostName;
	}

	public String getConnectionStringIoTHub() {
		return connectionStringIoTHub;
	}

	public void setConnectionStringIoTHub(String connectionStringIoTHub) {
		this.connectionStringIoTHub = connectionStringIoTHub;
	}
}

class EventCallback1 implements IotHubEventCallback {
	@Override
	public void execute(IotHubStatusCode statusCode, Object context) {
		//System.out.println("IoT Hub responded to message with status " + statusCode.name());

		if (context != null) {
			synchronized (context) {
				context.notify();
			}
		}
	}
}