package com.ey.iot.iotsphereeg.events;

import org.springframework.context.ApplicationEvent;


public class NetworkExceptionEvent extends ApplicationEvent {

	private String message;

	public NetworkExceptionEvent(Object source, String message) {
		super(source);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
