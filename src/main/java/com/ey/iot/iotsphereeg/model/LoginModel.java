package com.ey.iot.iotsphereeg.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name="LOGIN")
public class LoginModel implements Serializable {

  /**
	 * 
	 */
	private static final long serialVersionUID = 6964050379580784301L;
private int Id;
  @Column(name="R_Cre_Time", nullable=false)
  private Date rcretime;
  @NotEmpty
  @Id
  @Column(name="Loginid", nullable=true)
  private String Loginid;
  @Column(name="Freefld1", nullable=false)
  private String Freefld1;
  @Column(name="Freefld2", nullable=false)
  private String Freefld2;
  @Column(name="Freefld3", nullable=true)
  private String Freefld3;
  @Column(name="R_Mod_Time", nullable=true)
  private Date RModTime;
  
  @Column(name="PWD", nullable=true)
  private String pwd;
  @Column(name="Name", nullable=true)
  private String name;
  @Column(name="City", nullable=true)
  private String city;
  @Column(name="Country", nullable=true)
  private String country;
  @Column(name="Address", nullable=true)
  private String address;

  

public String getPwd() {
	return pwd;
}

public void setPwd(String pwd) {
	this.pwd = pwd;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}


  
  
  public Date getRcretime()
  {
    return this.rcretime;
  }
  
  public void setRcretime(Date rcretime)
  {
    this.rcretime = rcretime;
  }
  
  public String getFreefld1()
  {
    return this.Freefld1;
  }
  
  public void setFreefld1(String freefld1)
  {
    this.Freefld1 = freefld1;
  }
  
  public String getFreefld2()
  {
    return this.Freefld2;
  }
  
  public void setFreefld2(String freefld2)
  {
    this.Freefld2 = freefld2;
  }
  
  public String getFreefld3()
  {
    return this.Freefld3;
  }
  
  public void setFreefld3(String freefld3)
  {
    this.Freefld3 = freefld3;
  }
  
 
  public Date getRModTime()
  {
    return this.RModTime;
  }
  
  public void setRModTime(Date RModTime)
  {
    this.RModTime = RModTime;
  }
  
 
  
  public String getLoginid()
  {
    return this.Loginid;
  }
  
  public void setLoginid(String loginid)
  {
    this.Loginid = loginid;
  }
  
  
  public int getId()
  {
    return this.Id;
  }
  
  public void setId(int id)
  {
    this.Id = id;
  }
  
  public boolean equals(Object obj)
  {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof LoginModel)) {
      return false;
    }
    LoginModel other = (LoginModel)obj;
    if (this.Id != other.Id) {
      return false;
    }
    return true;
  }
  


public static long getSerialversionuid() {
	return serialVersionUID;
}

@Override
public String toString() {
	return "LoginModel [Id=" + Id + ", rcretime=" + rcretime + ", Loginid=" + Loginid
			+", Freefld1=" + Freefld1 + ", Freefld2=" + Freefld2 + ", Freefld3=" + Freefld3
			+ ", RModTime=" + RModTime + ", pwd=" + pwd + ", name=" + name + ", city=" + city
			+ ", country=" + country + ", address=" + address + 
			 "]";
}


}
