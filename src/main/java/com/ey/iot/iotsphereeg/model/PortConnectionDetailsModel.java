package com.ey.iot.iotsphereeg.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "PORT_CONNECTION_DETAILS")
public class PortConnectionDetailsModel implements Serializable {

	/**
	 * PortConnectionDetailsModel
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "Id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;

	@Column(name = "Gateway_Id", nullable = false)
	private String gatewayId;

	@Column(name = "Port", unique = true,nullable = false)
	private String port;

	@Column(name = "Login_Id", nullable = true)
	private String loginId;

	@Column(name = "Connection_Status", nullable = true)
	private String connectionStatus;

	@Column(name = "Connection_Start_Time", nullable = true)
	private String connectionStartTime;

	@Column(name = "Connection_End_Time", nullable = true)
	private String connectionEndTime;

	@ColumnDefault("'false'")
	@Column(name = "Del_Flg", nullable = false)
	private boolean Del_Flg;

	@Column(name = "R_Cre_Time", nullable = true)
	private String rCreTime;

	@Column(name = "R_Mod_Time", nullable = true)
	private String rModTime;

	public int getId() {
		return Id;
	}

	@Override
	public String toString() {
		return "PortConnectionDetailsModel [Id=" + Id + ", gatewayId=" + gatewayId + ", port=" + port + ", loginId="
				+ loginId + ", connectionStatus=" + connectionStatus + ", connectionStartTime=" + connectionStartTime
				+ ", connectionEndTime=" + connectionEndTime + ", Del_Flg=" + Del_Flg + ", rCreTime=" + rCreTime
				+ ", rModTime=" + rModTime + "]";
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PortConnectionDetailsModel)) {
			return false;
		}
		PortConnectionDetailsModel other = (PortConnectionDetailsModel) obj;
		if (this.Id != other.Id) {
			return false;
		}
		return true;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getConnectionStatus() {
		return connectionStatus;
	}

	public void setConnectionStatus(String connectionStatus) {
		this.connectionStatus = connectionStatus;
	}

	public String getConnectionStartTime() {
		return connectionStartTime;
	}

	public void setConnectionStartTime(String connectionStartTime) {
		this.connectionStartTime = connectionStartTime;
	}

	public String getConnectionEndTime() {
		return connectionEndTime;
	}

	public void setConnectionEndTime(String connectionEndTime) {
		this.connectionEndTime = connectionEndTime;
	}

	public boolean isDel_Flg() {
		return Del_Flg;
	}

	public void setDel_Flg(boolean del_Flg) {
		Del_Flg = del_Flg;
	}

	public String getrCreTime() {
		return rCreTime;
	}

	public void setrCreTime(String rCreTime) {
		this.rCreTime = rCreTime;
	}

	public String getrModTime() {
		return rModTime;
	}

	public void setrModTime(String rModTime) {
		this.rModTime = rModTime;
	}

}
