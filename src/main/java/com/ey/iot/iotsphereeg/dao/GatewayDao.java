package com.ey.iot.iotsphereeg.dao;

import java.util.List;

import com.ey.iot.iotsphereeg.model.GatewayModel;

public abstract interface GatewayDao {
	public abstract List<GatewayModel> getAllGateway();

	public abstract List<GatewayModel> getGatewayById(String GatewayId);

	public abstract void addGateway(GatewayModel gateway);

	public abstract void updategateway(GatewayModel gateway);

	public abstract void deletegateway(String gatewayId);

	public abstract boolean gatewayExists(String gatewayid);

	public abstract List<GatewayModel> searchGateway(String keyword);

	public abstract GatewayModel getGateway(String gatewayID);
	
	public abstract void saveOrUpdate(GatewayModel gateway);
}