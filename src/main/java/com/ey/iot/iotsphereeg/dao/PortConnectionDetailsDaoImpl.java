package com.ey.iot.iotsphereeg.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotsphereeg.model.PortConnectionDetailsModel;
import com.ey.iot.iotsphereeg.utility.Constants;

@Transactional
@Repository
public class PortConnectionDetailsDaoImpl implements PortConnectionDetailsDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public void saveOrUpdate(PortConnectionDetailsModel portConnectionDetails) {

		PortConnectionDetailsModel existingPortConnectionDetail = getExistingConnection(
				portConnectionDetails.getLoginId(), portConnectionDetails.getPort());

		if (null != existingPortConnectionDetail && existingPortConnectionDetail.getId() > 0) {

			portConnectionDetails.setId((this.getPortConnectionUniqueId(portConnectionDetails.getLoginId(),
					portConnectionDetails.getPort())).getId());

			portConnectionDetails.setrCreTime(existingPortConnectionDetail.getrCreTime());

			portConnectionDetails.setrModTime(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
            if(portConnectionDetails.getConnectionStatus()==Constants.CONNECTED_STATUS&&portConnectionDetails.getConnectionStartTime()!=null)
            {
			portConnectionDetails.setConnectionStartTime(portConnectionDetails.getConnectionStartTime());
            }
            else
            {
            	portConnectionDetails.setConnectionStartTime(existingPortConnectionDetail.getConnectionStartTime());
            }
			entityManager.merge(portConnectionDetails);

		} else {
			portConnectionDetails.setrCreTime(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date()));
			entityManager.persist(portConnectionDetails);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PortConnectionDetailsModel> getPortConnectionDetails( String port) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class))
				.createCriteria(PortConnectionDetailsModel.class)
				.add(Restrictions.eq("port", port))
				.addOrder(Order.asc( "connectionEndTime" ));
		return criteria.list();
	}

	public boolean isPortConnectionExists(String loginId, String port) {
		String hql = "FROM PortConnectionDetailsModel as pcdm WHERE pcdm.port = :port";
		int count = this.entityManager.createQuery(hql).setParameter("port", port)
				.getResultList().size();
		return count > 0;
	}

	public PortConnectionDetailsModel getExistingConnection(String loginId, String port) {

		String hql = "FROM PortConnectionDetailsModel as pcdm WHERE  pcdm.port = :port";

		List<PortConnectionDetailsModel> existingPortConnectionList = this.entityManager.createQuery(hql)
				.setParameter("port", port).getResultList();

		if (null != existingPortConnectionList && existingPortConnectionList.size() > 0) {
			return existingPortConnectionList.get(0);
		}
		return null;
	}

	public PortConnectionDetailsModel getPortConnectionUniqueId(String loginId, String port) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class))
				.createCriteria(PortConnectionDetailsModel.class)
				.add(Restrictions.eq("port", port));
		return (PortConnectionDetailsModel) criteria.list().get(0);
	}

	@Override
	public void updateAllPortStatusToDisconnected() {
		Query query;
		String connectionEndTime = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a").format(new Date());
		query = ((Session) this.entityManager.unwrap(Session.class))
				.createSQLQuery("UPDATE PORT_CONNECTION_DETAILS SET connection_status='" + Constants.DISCONNECTED_STATUS
						+ "',connection_end_time='" + connectionEndTime + "',r_mod_time='" + connectionEndTime + "'");
		query.executeUpdate();
	}

}
