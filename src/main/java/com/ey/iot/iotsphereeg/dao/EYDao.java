package com.ey.iot.iotsphereeg.dao;

import java.util.List;

import com.ey.iot.iotsphereeg.model.LoginModel;

public abstract interface EYDao
{  
  public abstract List<LoginModel> finddata(String paramString);

public abstract void saveOrUpdate(LoginModel loginmodel);

}
