package com.ey.iot.iotsphereeg.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotsphereeg.model.GatewayModel;

@Transactional
@Repository
public class GatewayDaoImp implements GatewayDao {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<GatewayModel> getAllGateway() {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(GatewayModel.class);
		criteria.addOrder(Order.asc("R_Cre_Time"));
		List<GatewayModel> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GatewayModel> getGatewayById(String GatewayId) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(GatewayModel.class);
		criteria.addOrder(Order.asc("R_Cre_Time"));
		if (GatewayId != "") {
			criteria.add(Restrictions.eq("Gateway_Id", GatewayId));
		}
		return criteria.list();
	}

	@Override
	public void addGateway(GatewayModel gateway) {
		Session sess = ((Session) this.entityManager.unwrap(Session.class));
		sess.saveOrUpdate(gateway);
	}

	@Override
	public void updategateway(GatewayModel gateway) {
		Session sess = ((Session) this.entityManager.unwrap(Session.class));
		sess.saveOrUpdate(gateway);

	}

	@Override
	public void deletegateway(String gatewayId) {
		List<GatewayModel> geteway = getGatewayById(gatewayId);
		Session sess = ((Session) this.entityManager.unwrap(Session.class));
		for (GatewayModel gw : geteway) {
			sess.delete(gw);
		}
	}

	@Override
	public boolean gatewayExists(String gatewayid) {
		List<GatewayModel> geteway = getGatewayById(gatewayid);
		int count = geteway.size();
		return count > 0 ? true : false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GatewayModel> searchGateway(String keyword) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(GatewayModel.class)
				.setProjection(Projections.projectionList().add(Projections.property("Gateway_Id"), "Gateway_Id"))
				.add(Restrictions.eq("Del_Flg", false)).add(Restrictions.like("Gateway_Id", "%" + keyword + "%"));
		return criteria.list();
	}

	@Override
	public GatewayModel getGateway(String gatewayID) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(GatewayModel.class)
				.add(Restrictions.eq("Gateway_Id", gatewayID));
		return (GatewayModel) criteria.list().get(0);
	}

	@Override
	public void saveOrUpdate(GatewayModel gateway) {
		if (this.gatewayExists(gateway.getGateway_Id())) {
			gateway.setId((this.getGateway(gateway.getGateway_Id())).getId());
			entityManager.merge(gateway);
		} else {
			gateway.setR_Cre_Time(gateway.getR_Mod_Time());
			entityManager.persist(gateway);
		}
	}

}
