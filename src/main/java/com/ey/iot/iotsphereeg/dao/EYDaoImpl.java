package com.ey.iot.iotsphereeg.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.annotations.DynamicUpdate;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ey.iot.iotsphereeg.model.LoginModel;

@Repository
@DynamicUpdate(true)

public class EYDaoImpl implements EYDao {

	@PersistenceContext
	private EntityManager entityManager;

	protected Session getSession() {
		return this.entityManager.unwrap(Session.class);
	}

	public void persist(Object entity) {
		getSession().persist(entity);
	}

	public void delete(Object entity) {
		getSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<LoginModel> finddata(String input) {
		Criteria criteria = getSession().createCriteria(LoginModel.class);
		criteria.addOrder(Order.desc("rcretime"));
		if (input != "") {
			criteria.add(Restrictions.eq("Loginid", input));
		}
		return criteria.list();
	}

	@Override
	public void saveOrUpdate(LoginModel LoginModel) {
		Session sess = getSession();
		sess.beginTransaction();
		sess.saveOrUpdate(LoginModel);
		// sess.getTransaction().commit();
	}
}
