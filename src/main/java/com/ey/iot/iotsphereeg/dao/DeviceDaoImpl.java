package com.ey.iot.iotsphereeg.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotsphereeg.model.DeviceModel;

@Transactional
@Repository
public class DeviceDaoImpl implements DeviceDao {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<DeviceModel> finddata(String input) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(DeviceModel.class);
		criteria.addOrder(Order.desc("R_Cre_Time"));
		if (input != "") {
			criteria.add(Restrictions.eq("Device_Id", input));
		}
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<DeviceModel> checkregistration(String input) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(DeviceModel.class);
		criteria.addOrder(Order.desc("R_Cre_Time"));
		if (input != "") {
			criteria.add(Restrictions.eq("Device_Id", input));
		}
		return criteria.list();
	}

	public void deletedata(String ssn) {
		Query query;
		query = ((Session) this.entityManager.unwrap(Session.class)).createSQLQuery(
				"DELETE FROM JYMAIN WHERE R_CRE_TIME in (SELECT TOP 3 R_CRE_TIME FROM JYMAIN ORDER BY R_CRE_TIME ASC)");
		query.executeUpdate();
	}

	public void updateallfirmware(String status) {
		Query query;
		query = ((Session) this.entityManager.unwrap(Session.class))
				.createSQLQuery("UPDATE ALERTCONFIG SET FreeFld2='" + status + "'");
		query.executeUpdate();
	}

	@Override
	@Transactional
	public void saveOrUpdate(DeviceModel DeviceModel) {
		// Session sess = ((Session) this.entityManager.unwrap(Session.class));
		// sess.saveOrUpdate(DeviceModel);
		if (this.DeviceExists(DeviceModel.getDevice_Id())) {
			DeviceModel.setId((this.getDeviceByDeviceId(DeviceModel.getDevice_Id())).getId());
			entityManager.merge(DeviceModel);
		} else {
			DeviceModel.setR_Cre_Time(DeviceModel.getR_Mod_Time());
			entityManager.persist(DeviceModel);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeviceModel> findall() {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(DeviceModel.class);
		criteria.addOrder(Order.desc("R_Cre_Time"));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeviceModel> searchDevice(String keyword) {
		Disjunction or = Restrictions.disjunction();
		or.add(Restrictions.like("Device_Id", "%" + keyword + "%"));
		or.add(Restrictions.like("Gateway_Id", "%" + keyword + "%"));
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(DeviceModel.class)
				.setProjection(Projections.projectionList().add(Projections.property("Device_Id"), "Device_Id")
						.add(Projections.property("Gateway_Id"), "Device_Id"))
				.add(or).add(Restrictions.eq("Del_Flg", false));
		;
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeviceModel> getDeviceByGatewayId(String gateway) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(DeviceModel.class)
				/*.setProjection(Projections.projectionList().add(Projections.property("Device_Id"), "Device_Id")
						.add(Projections.property("Gateway_Id"), "Device_Id"))*/
				.add(Restrictions.eq("Gateway_Id", gateway)).add(Restrictions.eq("Del_Flg", false));
		;
		return criteria.list();
	}

	@Override
	public DeviceModel getDeviceByDeviceId(String deviceid) {
		Criteria criteria = ((Session) this.entityManager.unwrap(Session.class)).createCriteria(DeviceModel.class)
				.add(Restrictions.eq("Device_Id", deviceid));
		return (DeviceModel) criteria.list().get(0);
	}

	public void addDevice(DeviceModel Device) {
		this.entityManager.persist(Device);
	}

	public boolean DeviceExists(String Deviceid) {
		String hql = "FROM DeviceModel as atcl WHERE atcl.Device_Id = ?";
		int count = this.entityManager.createQuery(hql).setParameter(1, Deviceid).getResultList().size();
		return count > 0;
	}

}
