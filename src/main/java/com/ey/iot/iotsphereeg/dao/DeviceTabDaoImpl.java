package com.ey.iot.iotsphereeg.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotsphereeg.model.DeviceTabModel;

@Transactional
@Repository
public class DeviceTabDaoImpl implements DeviceTabDao {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<DeviceTabModel> finddata(String input) {
		Criteria criteria = ((Session)this.entityManager.unwrap(Session.class)).createCriteria(DeviceTabModel.class);
		criteria.addOrder(Order.desc("rcretime"));
		if (input != "") {
			criteria.add(Restrictions.eq("Deviceid", input));
			criteria.addOrder(Order.asc("id"));
		}
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<DeviceTabModel> checkregistration(String input) {
		Criteria criteria = ((Session)this.entityManager.unwrap(Session.class)).createCriteria(DeviceTabModel.class);
		criteria.addOrder(Order.desc("rcretime"));
		if (input != "") {
			criteria.add(Restrictions.eq("Deviceid", input));
		}
		return criteria.list();
	}

	public void deletedata(String ssn) {
		//String databasetype = this.env.getProperty("jdbc.databasetype");
		//System.out.println(databasetype);
		Query query;
		//if (databasetype.equalsIgnoreCase("MYSQL")) {
		//	query = ((Session)this.entityManager.unwrap(Session.class)).createSQLQuery("DELETE FROM JYMAIN");
		//} else {
			query = ((Session)this.entityManager.unwrap(Session.class)).createSQLQuery(
					"DELETE FROM JYMAIN WHERE R_CRE_TIME in (SELECT TOP 3 R_CRE_TIME FROM JYMAIN ORDER BY R_CRE_TIME ASC)");
		//}
		query.executeUpdate();
	}

	public void updateallfirmware(String status) {
		//String databasetype = this.env.getProperty("jdbc.databasetype");
		//System.out.println(databasetype);
		Query query;
		query = ((Session)this.entityManager.unwrap(Session.class)).createSQLQuery("UPDATE ALERTCONFIG SET FreeFld2='" + status + "'");
		query.executeUpdate();
	}

	@Override
	public void saveOrUpdate(DeviceTabModel DeviceTabModel) {
		// TODO Auto-generated method stub
		Session sess = ((Session)this.entityManager.unwrap(Session.class));
		sess.saveOrUpdate(DeviceTabModel);
	}
}
