package com.ey.iot.iotsphereeg.dao;

import java.util.List;

import com.ey.iot.iotsphereeg.model.DeviceTabModel;

public abstract interface DeviceTabDao
{  
  public abstract List<DeviceTabModel> finddata(String paramString);
  
  public abstract void deletedata(String paramString);
  public abstract void updateallfirmware(String status);
public abstract void saveOrUpdate(DeviceTabModel jymodel);

public abstract List<DeviceTabModel> checkregistration(String input);
}
