<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- <div id="home" ng-controller="homeCtrl"> -->
<div id="layoutContainer" layout="column" ng-cloak layout-wrap>
	<div flex="10">
		<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
			<div flex="100" class="box1">Home</div>
		</div>
	</div>
	<div flex="90"></div>
</div>
<!-- Header in top div -->
<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
	<div flex="100" class="box1" data-ng-init="liveDataInitializeData('IoT_01')" align="center">
		<!-- <md-button ng-click=showhide()> <span
			class="menu-icon fa fa-bars"></span> Show and Hide Tables</md-button> -->
		<!-- <md-button ng-click="new_chart_dialogbox()"> <span
			class="menu-icon fa fa-line-chart"></span> Add Chart</md-button> -->
		<md-button ng-click="refresh()"> <span
			class="menu-icon fa fa-refresh"></span> Refresh</md-button>
		<md-button ng-click="d_manage()"> <span
			class="menu-icon fa fa-table"></span> Manage</md-button>
		<!-- <md-button ng-click="d_settings()"> <span
			class="menu-icon fa fa-table"></span> Settings</md-button> -->
		<!-- <md-button> <span class="menu-icon fa fa-file-pdf-o"></span>
							PDF</md-button> -->
		<md-button ng-click="d_export()"> <span
			class="menu-icon fa fa-cart-plus"></span> Export</md-button>
			
		<md-button ng-disabled="true" 
			ng-style="changeColor(deviceStatus)">{{deviceStatus}}</md-button>
			
			<select ng-model='comp.port' 
			ng-init='comp.port = "TestPort"' 
			ng-options="o as o for o in port.port"
				style="border-radius: 4px; border: 2px solid #AAAAAA; width: 8%">
				      <!-- <option label="TestPort" value="string:TestPort" selected="selected">TestPort</option> -->
				</select>&nbsp&nbsp&nbsp&nbsp
				
			<!-- <md-button class="md-raised material-cust-medium-button"
				ng-click="connect_disconnect('IoT_01',currentDeviceIndex)" ng-disabled="conn_but_dis">{{connect_but}}</md-button> -->

		<button type="button" class="btn btn-sm"
			ng-click="connect_disconnect('IoT_01',currentDeviceIndex);refresh()"
			ng-disabled="conn_but_dis">{{connect_but | uppercase}}</button>&nbsp&nbsp&nbsp&nbsp

		<!-- <timer interval="1000">{{hours}}:{{minutes}}:{{seconds}}</timer> -->
		<md-button ng-disabled="true">{{deviceConnectedTime}}</md-button>
		
	</div>
</div>
<!-- End Header in top div -->

<br />
<br />
<div class="container-flued">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<img src="dashboard/img/ajax-loading.gif" ng-show="loading" />
			<table style="width: 100%;"
				class="table  table-hover data-table sort display"
				<%-- ng-init='table_gateways = ${gateways }' --%>ng-hide="loading">
				<caption><h4><b>Gateways</b></h4></caption>
				<thead>
					<tr>
						<th><b>Sr.No</b></th>
						<th><b>Gateways</b></th>
					</tr>
				</thead>
				<tbody ng-repeat="g in table_gateways.gateways">
					<tr ng-click="go_gateway(g)">
						<td>{{$index+1}}</td>
						<td>{{g}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12" ng-show="notshow()">
			<H4>Notifications</H4>
			<table>
				<tr>
					<td><img ng-src="dashboard/img/{{notification.icon}}"
						height="42" width="42" /> &nbsp;&nbsp;&nbsp;</td>
					<td><table>
							<tr>
								<td>Code: {{notification.Status}}</td>
							</tr>
							<tr>
								<td>{{notification.Remark}}</td>
							</tr>
						</table></td>
				</tr>
			</table>
			<!-- </div> -->
		</div>
	</div>
</div>
<!-- </div> -->