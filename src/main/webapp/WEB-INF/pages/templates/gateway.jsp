<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- Top -->
<div class="top" ng-style="theight">

	<!-- Header in top div -->
	<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
		<div flex="100" class="box1">
			<div id="layoutContainer" layout="row" ng-cloak layout-wrap
				layout-align="start">
				<a href="#">Home</a> > <a href="#">Gateway</a> > {{gatewayid}}
			</div>
		</div>
		<div flex="100" class="box1">
			<md-button ng-click=showhide()> <span
				class="menu-icon fa fa-bars"></span> Show and Hide Tables</md-button>
			<!-- <md-button ng-click="d_settings()"> <span
				class="menu-icon fa fa-table"></span> Settings</md-button> -->
			<!-- <md-button> <span class="menu-icon fa fa-file-pdf-o"></span>
							PDF</md-button> -->
			<md-button ng-click="d_export()"> <span
				class="menu-icon fa fa-cart-plus"></span> Export</md-button>
		</div>
	</div>
	<!-- End Header in top div -->

	<!-- Tab Pane -->
	<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
		<div class="container-flued">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-xs-12">
					<img src="dashboard/img/ajax-loading.gif" ng-show="loading" />
					<table style="width: 100%;"
						class="table  table-hover data-table sort display"
						<%-- ng-init='table_devices = ${devices }' --%>ng-hide="loading">
						<caption>Devices</caption>
						<thead>
							<tr>
								<th>Sr.Id</th>
								<th>Device</th>
								<th>Gateway</th>
								<th>Added on</th>
								<th>Product Id</th>
								<th>Firmware Id</th>
							</tr>
						</thead>
						<tbody ng-repeat="d in table_devices.devices">
							<tr ng-click="go_device(d['Device_Id'],'','')">
								<td>{{$index+1}}</td>
								<td>{{d["Device_Id"]}}</td>
								<td>{{d["Gateway_Id"]}}</td>
								<td>{{d["R_Cre_Time"]}}</td>
								<td>{{d.Tag.productid}}</td>
								<td>{{d.Tag.firmver}}</td>
								<!-- <td><a ng-click="toggleSidebar()"> <span
								class="menu-icon glyphicon glyphicon-folder-open"></span></a>&nbsp;<span
							class="menu-icon glyphicon glyphicon glyphicon-trash"></span>&nbsp;<span
							class="menu-icon glyphicon glyphicon glyphicon-edit"></span></td> -->
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- End Tab Pane -->

</div>
<!-- End Top -->

<!-- Bottom -->

<div class="bottom" ng-show=sh>
	<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
		<div flex="100" class="box2">
			<md-button> <span class="menu-icon fa fa-trash"></span>
			Delete </md-button>
			<md-button> <span class="menu-icon fa fa-upload"></span>
			Update </md-button>
			<md-button> <span class="menu-icon fa fa-retweet"></span>
			Reload </md-button>
			<md-button> <span class="menu-icon fa fa-share-square-o"></span>
			Restore </md-button>
			<md-button> <span class="menu-icon fa fa-refresh"></span>
			Refresh </md-button>
		</div>
		<div flex="100" class="box1">
			<b>Gateway Info</b>
		</div>
	</div>
	<div class="md-padding">
		<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
			<div flex="33">
				<table class="table">
					<tr>
						<td><b>Status:</b></td>
						<td ng-style="changeColor('Green')">Online</td>
					</tr>
					<tr>
						<td><b>Alarm Status:</b></td>
						<td>Enabled</td>
					</tr>
					<tr>
						<td><b>Product ID:</b></td>
						<td>{{gatewaydata.gatewaydata.Tag.productid}}</td>
					</tr>
					<tr>
						<td><b>Upload data:</b></td>
						<td>10kb</td>
					</tr>
					<tr>
						<td><b>Download Data:</b></td>
						<td>100kb</td>
					</tr>
				</table>
			</div>
			<div flex="33">
				<table class="table">
					<tr>
						<td><b>Primary IP:</b></td>
						<td>{{gatewaydata.ipaddress}}</td>
					</tr>
					<tr>
						<td><b>Primary Hostname:</b></td>
						<td>{{gatewaydata.hostname}}</td>
					</tr>
					<tr>
						<td><b>Physical Mac Address:</b></td>
						<td>A0:32:2D:EB:43</td>
					</tr>
					<tr>
						<td><b>Firmware Version:</b></td>
						<td>1.0.0</td>
					</tr>
					<tr>
						<td><b>Operating Band:</b></td>
						<td>2.4 Ghz</td>
					</tr>
				</table>
			</div>
			<div flex>
				<table class="table">
					<tr>
						<td><b>Connection Time:</b></td>
						<td><timer interval="1000">{{hours}}:{{minutes}}:{{seconds}}</timer></td>
					</tr>
					<tr>
						<td><b>Type:</b></td>
						<td>{{gatewaydata.gatewaydata.Tag.type}}</td>
					</tr>
					<tr>
						<td><b>Data Rate:</b></td>
						<td>105 bps</td>
					</tr>
					<tr>
						<td><b>Health:</b></td>
						<td><div ng-style="changeColor('Green')">GOOD</div></td>
					</tr>
					<tr>
						<td><b>Power:</b></td>
						<td>5V</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- End Bottom -->