<!doctype html>
<html lang="en" ng-app="IoTSphereEG">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>IoTSphere EG | Dashboard</title>
<!-- STYLES -->
<link rel="stylesheet" href="dashboard/lib/css/main.min.css" />
<link rel="stylesheet" href="dashboard/lib/css/angular-material.min.css" />
<link rel="stylesheet" href="dashboard/lib/css/nv.d3.css" />
<link rel="stylesheet"
	href="dashboard/lib/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="dashboard/lib/css/ngDialog.css">
<link rel="stylesheet" href="dashboard/lib/css/bootstrap.min.css">
<link rel="stylesheet" href="dashboard/lib/css/style.css" />
<!-- SCRIPTS -->
<script src="dashboard/lib/js/main.min.js"></script>
<script src="dashboard/lib/js/angular-material.min.js"></script>
<script src="dashboard/lib/js/angular-animate.min.js"></script>
<script src="dashboard/lib/js/angular-aria.min.js"></script>
<script src="dashboard/lib/js/angular-messages.min.js"></script>
<script src="dashboard/lib/js/angular-dynamic-layout.min.js"></script>
<script src="dashboard/lib/js/d3.min.js"></script>
<script src="dashboard/lib/js/nv.d3.min.js"></script>
<script src="dashboard/lib/js/angular-nvd3.min.js"></script>
<script src="dashboard/lib/js/angular-sanitize.js"></script>
<script src="dashboard/lib/js/ngDialog.min.js"></script>
<script src="dashboard/lib/js/angular-timer.min.js"></script>
<script src="dashboard/lib/js/moment.min.js"></script>
<script src="dashboard/lib/js/humanize-duration.js"></script>
<script src="dashboard/lib/js/jquery.min.js"></script>
<script src="dashboard/lib/js/jquery.min.js"></script>
<script src="dashboard/lib/js/bootstrap.min.js"></script>
<script src="dashboard/lib/js/angular.easypiechart.js"></script>
<script src="dashboard/lib/js/sockjs.min.js"></script>
<script src="dashboard/lib/js/stomp.min.js"></script>
<!-- Custom Scripts -->
<script type="text/javascript"
	src="dashboard/js/master.js?t=${timestamp}"></script>
<script type="text/javascript"
	src="dashboard/js/config.js?t=${timestamp}"></script>
<script type="text/javascript"
	src="dashboard/js/service.js?t=${timestamp}"></script>
<script type="text/javascript"
	src="dashboard/js/directive.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/newdevice.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/newgateway.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/home.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/gateway.js?t=${timestamp}"></script>
	<%-- <script type="text/javascript"
	src="dashboard/js/devicectl.js?t=${timestamp}"></script> --%>
	
<%@include file="templates/popups.jsp"%>
<%@include file="templates/timeOut.jsp"%>

</head>
<body ng-controller="MasterCtrl" data-ng-init="liveDataInitializeData('IoT_01')" onload="StartTimers(${appMaxIdleTime});" onmousemove="ResetTimers(${appMaxIdleTime});">
	<!--  data-ng-init='getDataFromServer("${data}")' -->
	<div id="page-wrapper" ng-class="{'open': toggle}" ng-cloak>

		<!-- Sidebar -->
		<div id="sidebar-wrapper">

			<div class="sidebar">
				<div class="sidebar-main">
					<div class="md-padding">
						<div class="meta">
							<table style="width: 100%;">
								<tr>
									<td><img src="dashboard/img/EY-logo-horizontal.png"
										height=50px /></td>
									<td><br /> <a ng-click="toggleSidebar()"> <span
											class="menu-icon glyphicon glyphicon-transfer"
											style="color: #FFFFFF; font-size: 150%;"></span></a></td>
								</tr>
							</table>
						</div>
						<br /> <br />
						<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
							<div flex="90">
								<input type='textbox' id="searchKey" ng-model="searchKey.text"
									name="searchKey" required
									placeholder="Search Device or Gateway"
									style="border: 1px solid grey; background-color: black; width: 95%;" />
							</div>
							<div flex>
								<a ng-click="search()"><span
									class="menu-icon glyphicon glyphicon-search"
									style="color: #FFFFFF; font-size: 150%;"></span></a>
							</div>
						</div>
						<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
							{{searchKey.status}}</div>
					</div>
				</div>
				<div ng-repeat="device in devices">
					<div ng-if="$index< 5">
						<uib-accordion close-others="true">
						<div uib-accordion-group class="device-menu panel-default "
							is-open="status.isFirstOpen" is-disabled="status.isFirstDisabled">
							<uib-accordion-heading> <b
								ng-style="{'color': device.color, 'padding-left': device.pad}">{{device.name}}</b>
							<span class="pull-right menu-icon fa fa-usb"
								ng-style="{'color': device.color, 'padding-right': device.padr}"></span><!-- <i
						class="pull-right glyphicon"
						ng-class="{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}"
						style="color: #E6E6DF; right: 18px;"></i> --> </uib-accordion-heading>
							<div id="layoutContainer" layout="row" ng-cloak layout-wrap
								ng-repeat="item in device.items">
								<div flex="90">
									<a href="{{item.url}}"
										ng-click="devchange($parent.$index,item.url)">{{item.name}}</a>
								</div>
								<div flex>
									<a href="{{item.url}}"
										ng-click="devchange($parent.$index,item.url)"><span
										class="menu-icon fa fa-{{item.icon}}"></span></a>
								</div>
							</div>
						</div>
						</uib-accordion>
					</div>
					<div ng-if="$index ==5">
						<a style="padding-left: 190px;" id="displayMoreDevicesId"
							ng-click="displayMoreDevices()"> ...More </a>
					</div>
				</div>
			</div>

			<div class="sidebar-footer">
				<div class="col-xs-4">
					<a href="#/support"> Support </a>
				</div>
				<div class="col-xs-4">
					<a href="#/about"> About </a>
				</div>
				<div class="al-copy">
					<a href="https://www.ey.com" target="_blank"> &copy; EY 2017 </a>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->
		<!-- Content-wrapper -->
		<div id="content-wrapper">
			<!-- Page Content -->
			<div class="page-content">
				<div class="menu">
					<nav class="navbar navbar-inverse"
						style="margin-bottom: 0px; border-radius: 0px; border: 0px solid">
						<div class="container" style="width: 100%;">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed"
									data-toggle="collapse"
									data-target="#bs-example-navbar-collapse-1"
									aria-expanded="false">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
								<a class="navbar-brand" href="#" style="color: #FFF;">IoTSphere
									Edge Gateway</a>
							</div>
							
							<h3>${errorMessage}</h3>
							
							<div class="collapse navbar-collapse"
								id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav navbar-right">
									<li class="dropdown" style="cursor: pointer; float: right;"><a
										class="dropdown-toggle" ng-style="notstyle"
										data-toggle="dropdown" ng-click="notcolor()"><span
											class="menu-icon glyphicon glyphicon-bell"></span></a>
										<ul class="dropdown-menu" style="padding: 12px 16px;">
											<li><B>Notifications</B></li>
											<li ng-show="notshow()"><a href="#/"><table>
														<tr>
															<td><img
																ng-src="dashboard/img/{{notification.icon}}" height="42"
																width="42" /> &nbsp;&nbsp;&nbsp;</td>
															<td><table>
																	<tr>
																		<td>Code: {{notification.Status}}</td>
																	</tr>
																	<tr>
																		<td>{{notification.Remark}}</td>
																	</tr>
																</table></td>
														</tr>
													</table></a></li>
										</ul></li>
									<li><a href="#/" style="color: #FFF;"><span
											class="fa fa-home"></span> Home</a></li>
									<li><a href="#/support" style="color: #FFF;"><span
											class="fa fa-phone"></span> Support</a></li>
									<!-- <li><a ng-click="d_bulkupload()" style="color:#FFF;"><span class="fa fa-info"></span>
											Bulk Upload</a></li> -->
									<li><a href="#/about" style="color: #FFF;"><span
											class="fa fa-info"></span> About</a></li>
									<li><a href="logout" style="color: #FFF;"><span
											class="fa fa-sign-out"></span> Logout</a></li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
				<div ui-view class="contentpan"></div>
			</div>
			<!-- End Page Content -->
		</div>
		<!-- End Content Wrapper -->
	</div>
	<!-- End Page Wrapper -->
	
</body>
</html>